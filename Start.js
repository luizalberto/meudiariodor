import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import Resumo from "./src/components/Resumo";
import Iniciativa from "./src/screens/Iniciativa";
import Lembrete from "./src/screens/Lembrete";
import Menu from "./src/screens/Menu";
import PdfScreen from "./src/screens/PdfScreen";
import ScreenDiario from "./src/screens/ScreenDiario";
import ScreenDorNaCabeca from "./src/screens/ScreenDorNaCabeca";
import ScreenDorNoCorpo from "./src/screens/ScreenDorNoCorpo";
import ScreenHitorico from "./src/screens/ScreenHistorico";
import ScreenRelatorio from "./src/screens/ScreenRelatorio";
import ScreenSaibaMais from "./src/screens/ScreenSaibaMais";
import Splash from "./src/screens/Splash";
import AppColors from "./src/styles/AppColors";
import Intro from "./src/screens/Intro";
import LembreteRoot from "./src/screens/LembreteRoot";

const RootStack = createStackNavigator(
  {
    Splash: {
      screen: Splash
    },
    Iniciativa: {
      screen: Iniciativa
    },
    Intro: {
      screen: Intro
    },
    Menu: {
      screen: Menu
    },
    Diario: {
      screen: ScreenDiario
    },
    LembreteRoot: {
      screen: LembreteRoot
    },
    SaibaMais: {
      screen: ScreenSaibaMais
    },
    Corpo: {
      screen: ScreenDorNoCorpo
    },
    Cabeca: {
      screen: ScreenDorNaCabeca
    },
    Relatorio: {
      screen: ScreenRelatorio
    },
    Historico: {
      screen: ScreenHitorico
    },
    Resumo: {
      screen: Resumo
    },
    Pdf: {
      screen: PdfScreen
    }
  },
  {
    initialRouteName: "Splash",
    navigationOptions: {
      headerStyle: {
        backgroundColor: AppColors.BLUE
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: "300"
      }
    }
  }
);

export default class App extends Component {
  render() {
    return <RootStack />;
  }
}
