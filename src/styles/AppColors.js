
export default class AppColors {
    static DARK_BLUE = "#213f61";
    static BLUE = "#57b3e3";

    static ORANGE = "#f3c342";
    static ORANGE_DARK = "#f1b821";

    static DARK = "#333333"
    static RED = "#820f05";    
    static WHITE = "#FFFFFF";

    static BLUE0 = "#8acaed";
    static BLUE1 = "#58b4e5";
    static BLUE2 = "#4b97c0";
    static BLUE3 = "#3b7a9c";
    static BLUE4 = "#295d79";
    static BLUE5 = "#113e53";

}
