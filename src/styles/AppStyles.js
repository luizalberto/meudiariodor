import { StyleSheet } from "react-native";
import AppMetrics from "./AppMetrics";
import AppColors from "./AppColors";

export default StyleSheet.create({
  titulo: {
    fontSize: AppMetrics.LG_TEXT,
    fontWeight: "bold",
    marginTop: 15,
    color: AppColors.DARK_BLUE
  },
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    paddingTop: 0
  },
  cover: {
    width: 300,
    height: 100,
    resizeMode: "cover"
  },
  dialog: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    paddingTop: 0
  },
   input: {
    color: "#000",
    fontSize: 16,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 38,
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 6,
    paddingRight: 6
  },
});
