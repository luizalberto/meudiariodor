
export default class AppMetrics {

  static BASE_GRID = 4;
  static SM_SPACING = 8;
  static MD_SPACING = 16;
  static LG_SPACING = 24;
  static XG_SPACING = 32;

  static SM_TEXT = 14;
  static MD_TEXT = 16;
  static LG_TEXT = 18;
  static XG_TEXT = 22;

  static ICON_SIZE = 30;
  static COVER = 250;

}
