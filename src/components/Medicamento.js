import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableHighlight,
  Picker
} from "react-native";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import AppStyles from "../styles/AppStyles";

export default class Medicamento extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.state,
      aliviomedicamento: this.props.state.dor.aliviomedicamento
    };
    this.updateState = this.updateState.bind(this);
    this.selecionaAlivio = this.selecionaAlivio.bind(this);
  }

  updateState(st) {
    this.props.updateState(st.data);
    this.setState(st);
  }

  selecionaAlivio(alivio) {
    var st = this.state;
    st.aliviomedicamento = alivio;
    st.data.dor.aliviomedicamento = alivio;
    this.updateState(st);
  }

  render() {
    var st = this.state;
    return (
      <View style={{ flex: 1, paddingLeft: 20 }}>
        <Text
          style={{
            textAlign: "center",
            marginTop: 15,
            color: "#fff",
            fontSize: AppMetrics.XG_TEXT
          }}
        >
          MEDICAMENTO
        </Text>

        <View style={{ flex: 1, flexDirection: "row", marginTop: 10 }}>
          <Text
            style={{
              marginTop: 6,
              textAlign: "left",
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            Nome
          </Text>

          <TextInput
            underlineColorAndroid="transparent"
            onSubmitEditing={() => {
              this.secondTextInput.focus();
            }}
            style={[AppStyles.input, { width: "77%" }]}
            returnKeyType="next"
            maxLength={90}
            value={st.data.dor.medicamentonome}
            onChangeText={value => {
              st.data.dor.medicamentonome = value;
              this.updateState(st);
            }}
          />
        </View>

        <View style={{ flex: 1, flexDirection: "row", marginTop: 10 }}>
          <Text
            style={{
              marginTop: 6,
              textAlign: "left",
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            Quantidade
          </Text>

          <TextInput
            underlineColorAndroid="transparent"
            ref={input => {
              this.secondTextInput = input;
            }}
            onSubmitEditing={() => {
              this.thirdTextInput.focus();
            }}
            style={[AppStyles.input, { width: "20%" }]}
            keyboardType="numeric"
            returnKeyType="next"
            maxLength={2}
            value={st.data.dor.medicamentoqtd}
            onChangeText={value => {
              st.data.dor.medicamentoqtd = value;
              this.updateState(st);
            }}
          />

          <View style={{
            width: "40%",
            height: 38,
            marginLeft: 10,
            borderRadius: 5, 
            overflow: 'hidden'
          }}>
            <Picker
              ref={input => {
                this.fourTextInput = input;
              }}
              onSubmitEditing={() => {
                this.fiveTextInput.focus();
              }}
              returnKeyType="next"
              selectedValue={st.data.dor.medicamentounidademedida}
              style={{
                height: 38,
                backgroundColor: "#fff",
              }}
              onValueChange={(itemValue, itemIndex) => {
                st.data.dor.medicamentounidademedida = itemValue;
                this.updateState(st);
              }}
            >
              <Picker.Item label="Selecione" value="" />
              <Picker.Item label="unidade" value="unidade" />
              <Picker.Item label="ml" value="ml" />
              <Picker.Item label="gotas" value="gotas" />
            </Picker>
          </View>
        </View>

        <View style={{ flex: 1, flexDirection: "row", marginTop: 10 }}>
          <Text
            style={{
              marginTop: 6,
              textAlign: "left",
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            Intervalo
          </Text>

          <View style={{
            width: "70%",
            height: 38,
            marginLeft: 10,
            borderRadius: 5, 
            overflow: 'hidden'
          }}>
          <Picker
            ref={input => {
              this.fiveTextInput = input;
            }}
            onSubmitEditing={() => {
              this.sixTextInput.focus();
            }}
            selectedValue={st.data.dor.medicamentointervalo}
            returnKeyType="next"
            style={{
              backgroundColor: "#fff",
              height:100,
              width:100,
              borderRadius: 5, 
            }}
            onValueChange={(itemValue, itemIndex) => {
              st.data.dor.medicamentointervalo = itemValue;
              this.updateState(st);
            }}
          >
            <Picker.Item label="Selecione" value="" />
            <Picker.Item
              label="Horário específico"
              value="Horário específico"
            />
            <Picker.Item label="4/4 horas" value="4/4 horas" />
            <Picker.Item label="6/6 horas" value="6/6 horas" />
            <Picker.Item label="8/8 horas" value="8/8 horas" />
            <Picker.Item label="12/12 horas" value="12/12 horas" />
            <Picker.Item label="1 vez ao dia" value="1 vez ao dia" />
          </Picker>
        </View>
        </View>

        <View style={{ flex: 1, flexDirection: "row", marginTop: 10 }}>
          <Text
            style={{
              marginTop: 6,
              textAlign: "center",
              color: "#fff",
              fontSize: AppMetrics.SM_TEXT
            }}
          >
            Horário da 1º dose
          </Text>

          <TextInput
            ref={input => {
              this.sixTextInput = input;
            }}
            onSubmitEditing={() => {
              this.sevenTextInput.focus();
            }}
            underlineColorAndroid="transparent"
            style={[AppStyles.input, {  textAlign: "center" }]}
            keyboardType="numeric"
            returnKeyType="next"
            maxLength={2}
            value={st.data.dor.horamedicamento}
            onChangeText={value => {
              st.data.dor.horamedicamento = value;
              this.updateState(st);
              if (value.length === 2) {
                this.sevenTextInput.focus();
              }
            }}
          />
          <Text style={styles.text}> : </Text>
          <TextInput
            ref={input => {
              this.sevenTextInput = input;
            }}
            underlineColorAndroid="transparent"
            style={[AppStyles.input, { textAlign: "center", marginLeft:0 }]}
            keyboardType="numeric"
            returnKeyType="done"
            maxLength={2}
            value={st.data.dor.minutomedicamento}
            onChangeText={value => {
              st.data.dor.minutomedicamento = value;
              this.updateState(st);
            }}
          />
        </View>

        <Text
          style={{
            textAlign: "center",
            marginTop: 15,
            color: "#fff",
            fontSize: AppMetrics.XG_TEXT
          }}
        >
          ALÍVIO DA DOR
        </Text>

        <View
          style={{
            flex: 1,
            flexDirection: "row",
            marginTop: 10,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <TouchableHighlight
            underlayColor="#00000000"
            onPress={this.onPress}
            activeOpacity={0.7}
            style={{ flex: 1 }}
            onPress={() => {
              this.selecionaAlivio(1);
            }}
          >
            <Image
              tintColor={
                this.state.aliviomedicamento == 1 ? AppColors.BLUE2 : "#fff"
              }
              source={require("../assets/smile5.png")}
              style={{
                flex: 1,
                height: 50,
                width: 55,
                alignItems: "center",
                resizeMode: "contain"
              }}
            />
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor="#00000000"
            onPress={this.onPress}
            activeOpacity={0.7}
            style={{ flex: 1 }}
            onPress={() => {
              this.selecionaAlivio(2);
            }}
          >
            <Image
              tintColor={
                this.state.aliviomedicamento == 2 ? AppColors.BLUE2 : "#fff"
              }
              source={require("../assets/smile3.png")}
              style={{
                flex: 1,
                height: 50,
                width: 55,
                alignItems: "center",
                resizeMode: "contain"
              }}
            />
          </TouchableHighlight>

          <TouchableHighlight
            underlayColor="#00000000"
            onPress={this.onPress}
            activeOpacity={0.7}
            style={{ flex: 1 }}
            onPress={() => {
              this.selecionaAlivio(3);
            }}
          >
            <Image
              tintColor={
                this.state.aliviomedicamento == 3 ? AppColors.BLUE2 : "#fff"
              }
              source={require("../assets/smile1.png")}
              style={{
                flex: 1,
                height: 50,
                width: 55,
                alignItems: "center",
                resizeMode: "contain"
              }}
            />
          </TouchableHighlight>
        </View>

        <View style={{ flex: 1, flexDirection: "column", marginTop: 15 }}>
          <Text
            style={{
              textAlign: "center",
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            Observações:
          </Text>

          <TextInput
            underlineColorAndroid="transparent"
            style={[AppStyles.input, { width: "95%", marginLeft: 0 }]}
            value={st.data.dor.observacaomedicamento}
            onChangeText={value => {
              st.data.dor.observacaomedicamento = value;
              this.updateState(st);
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  input: {
    color: "#000",
    fontSize: AppMetrics.MD_TEXT,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  },
  textSmall: {
    color: "#fff",
    fontSize: AppMetrics.SM_TEXT
  }
});
