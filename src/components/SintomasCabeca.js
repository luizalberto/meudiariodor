import React, { Component } from 'react';
import { View, Text ,TextInput, StyleSheet} from 'react-native';
import { CheckBox } from 'native-base'
import AppMetrics from '../styles/AppMetrics';

export default class SintomasCabeca extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: this.props.state,
    };
    this.updateState = this.updateState.bind(this);
    this.onChange = this.onChange.bind(this);
    this.checked = this.checked.bind(this);
  }

  updateState(st){
    this.props.updateState(st.data);
    this.setState(st);
  }
  onChange(chave, valor){
    var st = this.state;
    var index = st.data.dor[chave].indexOf(valor);
    if(index == -1){
        st.data.dor[chave].push(valor);
    }else{
        st.data.dor[chave].splice(index, 1);
    }
    this.updateState(st)
  }

  checked(chave, valor){
    var st = this.state;
    if(st.data.dor[chave] == undefined){
        st.data.dor[chave] = []
    }
    return st.data.dor[chave].indexOf(valor) != -1;
  }

  render() {
      var st = this.state;
    return (
        <View style={{flex:1, justifyContent:"center", paddingLeft:20}}>
            <Text style={{textAlign: "center", marginTop: 15, color: "#fff", fontSize: AppMetrics.XG_TEXT }}>
                FATORES DESENCADEANTES E SINTOMAS
            </Text>
            
            <View style={{flex:1, flexDirection:"column"}}>
                <Text style={{textAlign: "left", marginTop: 15, color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    O QUE INICIOU A CRISE:
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Falta de sono')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Falta de sono')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Falta de sono
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Excesso de horas dormidas')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Excesso de horas dormidas')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Excesso de horas dormidas
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Chocolate (falta ou excesso)')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Chocolate (falta ou excesso)')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Chocolate (falta ou excesso)
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Café (falta ou excesso)')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Café (falta ou excesso)')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Café (falta ou excesso)
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Embutidos e conservas (mortadela, salsicha etc.)')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Embutidos e conservas (mortadela, salsicha etc.)')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Embutidos e conservas 
                    (mortadela, salsicha etc.)
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Queijos')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Queijos')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Queijos
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Vinhos')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Vinhos')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Vinhos
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('inicio','Estresse emocional')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('inicio','Estresse emocional')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Estresse emocional
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Outros:
                </Text>
                <TextInput
                    underlineColorAndroid="transparent"
                    style={[styles.input, { width: '78%' }]}
                    value={st.data.dor.iniciooutros}
                    onChangeText={value => {
                    st.data.dor.iniciooutros = value;
                    this.updateState(st)
                    }}
                />
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />

            <View style={{flex:1, flexDirection:"column", marginTop:20}}>
                <Text style={{textAlign: "left", marginTop: 15, color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    SINTOMAS RELACIONADOS AO
                    INÍCIO DA CRISE:
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('iniciosintomas','Alterações visuais (moscas volantes, pontos cintilantes, visão dupla etc.)')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('iniciosintomas','Alterações visuais (moscas volantes, pontos cintilantes, visão dupla etc.)')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Alterações visuais (moscas
                    volantes, pontos cintilantes,
                    visão dupla etc.)
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('iniciosintomas','Perda do campo visual')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('iniciosintomas','Perda do campo visual')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Perda do campo visual
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('iniciosintomas','Perda de sensibilidade e/ou força em um dos membros')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('iniciosintomas','Perda de sensibilidade e/ou força em um dos membros')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Perda de sensibilidade e/ou 
                    força em um dos membros
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('iniciosintomas','Náuseas e vômitos')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('iniciosintomas','Náuseas e vômitos')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Náuseas e vômitos
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Outros:
                </Text>
                <TextInput
                    underlineColorAndroid="transparent"
                    style={[styles.input, { width: '78%' }]}
                    value={st.data.dor.iniciosintomasoutros}
                    onChangeText={value => {
                    st.data.dor.iniciosintomasoutros = value;
                    this.updateState(st)
                    }}
                />
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />

            <View style={{flex:1, flexDirection:"column", marginTop:20}}>
                <Text style={{textAlign: "left", marginTop: 15, color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    SINTOMAS PRESENTES NA CRISE:
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomaspresentes','Náuseas e vômitos')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('sintomaspresentes','Náuseas e vômitos')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Náuseas e vômitos
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomaspresentes','Intolerância ao ruído')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('sintomaspresentes','Intolerância ao ruído')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Intolerância ao ruído
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomaspresentes','Intolerância à luz')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('sintomaspresentes','Intolerância à luz')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Intolerância à luz
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomaspresentes','Intolerância à atividade física (com piora da dor)')} 
                        style={{marginRight:15}}
                        onPress={()=>{this.onChange('sintomaspresentes','Intolerância à atividade física (com piora da dor)')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Intolerância à atividade física
                    (com piora da dor)
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Outros:
                </Text>
                <TextInput
                    underlineColorAndroid="transparent"
                    style={[styles.input, { width: '78%' }]}
                    
                    value={st.data.dor.sintomasoutrospresente}
                    onChangeText={value => {
                    st.data.dor.sintomasoutrospresente = value;
                    this.updateState(st)
                    }}
                />
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "dashed",
                width:'95%',
                marginBottom:20
                }}
            />

        </View>
    );
  }
}

const styles = StyleSheet.create({
    div: {
      flexDirection: "row",
      paddingLeft: 15,
      paddingRight: 15
    },
    input: {
      color: "#000",
      fontSize: 18,
      backgroundColor: "#fff",
      borderRadius: 5,
      marginLeft: 10,
      height: 22,
      paddingTop: 3,
      paddingBottom: 3,
      paddingLeft: 6,
      paddingRight: 6
    },
    text: {
      color: "#fff",
      fontSize: AppMetrics.MD_TEXT
    },
    textSmall:{
        color: "#fff",
        fontSize:AppMetrics.SM_TEXT
    }
  });