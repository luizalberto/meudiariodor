import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  StyleSheet,
  Image,
  AsyncStoragem,
  TouchableHighlight
} from "react-native";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import AppStyles from "../styles/AppStyles";
import Dialog from "./Dialog";
import { Button } from "native-base";

const MES = [
  "Janeiro",
  "Fevereiro",
  "Março",
  "Abril",
  "Maio",
  "Junho",
  "Julho",
  "Agosto",
  "Setembro",
  "Outubro",
  "Novembro",
  "Dezembro"
];
const PERIODO = ["Manhã", "Tarde", "Dia inteiro", "Noite", "Madrugada"];
const INTENSIDADE = ["Manhã", "Tarde", "Dia inteiro", "Noite", "Madrugada"];

export default class Resumo extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Resumo"
  });

  constructor(props) {
    super(props);
    this.state = {
      relatorio: {},
      dores: [],
      dorAtual: {},
      dorAtualIndex: 0
    };
    this.getIconPeriodo = this.getIconPeriodo.bind(this);
    this.prev = this.prev.bind(this);
    this.next = this.next.bind(this);
    this.editar = this.editar.bind(this);
  }

  getIconPeriodo(icon) {
    switch (icon) {
      case 1:
        return require("../assets/icon_sunrise.png");
      case 2:
        return require("../assets/icon_sun.png");
      case 4:
        return require("../assets/icon_moon.png");
      case 5:
        return require("../assets/icon_star.png");
      default:
        return require("../assets/icon_saiba_mais.png");
    }
  }

  getIconIntensidade(icon) {
    switch (icon) {
      case 1:
        return require("../assets/smile1.png");
      case 2:
        return require("../assets/smile2.png");
      case 3:
        return require("../assets/smile3.png");
      case 4:
        return require("../assets/smile4.png");
      case 5:
        return require("../assets/smile5.png");
      case 6:
        return require("../assets/smile5.png");
      default:
        return require("../assets/icon_saiba_mais.png");
    }
  }

  getIconAlivio(icon) {
    switch (icon) {
      case 1:
        return require("../assets/smile5.png");
      case 2:
        return require("../assets/smile3.png");
      case 3:
        return require("../assets/smile1.png");
      default:
        return require("../assets/icon_saiba_mais.png");
    }
  }

  componentDidMount() {
    var st = this.state;
    st.dores = this.props.navigation.getParam("dores");
    st.dorAtual = st.dores[0];
    this.setState(st);
  }

  editar() {
    if (this.state.dorAtual.tipo == 1) {
      this.props.navigation.navigate("Corpo", {
        dor: this.state.dorAtual
      });
    } else if (this.state.dorAtual.tipo == 2) {
      this.props.navigation.navigate("Cabeca", {
        dor: this.state.dorAtual
      });
    }
  }

  prev() {
    var index = this.state.dorAtualIndex;
    var st = this.state;
    if (index > 0) {
      st.dorAtualIndex = index - 1;
      st.dorAtual = st.dores[st.dorAtualIndex];
      this.setState(st);
    }
  }

  next() {
    var index = this.state.dorAtualIndex;
    var st = this.state;
    if (index < st.dores.length - 1) {
      st.dorAtualIndex = index + 1;
      st.dorAtual = st.dores[st.dorAtualIndex];
      this.setState(st);
    }
  }

  render() {
    var dor = this.state.dorAtual;
    return (
      <ImageBackground
        style={AppStyles.container}
        source={require("../assets/app_bg.png")}
      >
        <Dialog
          borderColor={AppColors.BLUE2}
          bgColor={AppColors.BLUE1}
          height={"80%"}
          close={() => {
            this.props.navigation.navigate("Menu");
          }}
        >
          <View style={{ padding: 20 }}>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 10,
                marginBottom: AppMetrics.BASE_GRID
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                Data:
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  marginLeft: 5,
                  color: AppColors.BLUE5,
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                {dor.diaInicio +
                  " de " +
                  MES[Number.parseInt(dor.mesInicio) - 1] +
                  " de " +
                  dor.anoInicio}
              </Text>
            </View>
            <View
              style={{
                borderBottomColor: AppColors.BLUE5,
                borderBottomWidth: 1,
                borderStyle: "solid",
                width: "95%"
              }}
            />

            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "#fff",
                  fontWeight: "bold",
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                Medicamento:
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  color: AppColors.BLUE5,
                  marginLeft: 5,
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                {dor.medicamentonome}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                Observações:
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  color: AppColors.BLUE5,
                  marginLeft: 5,
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                {dor.observacaomedicamento}
              </Text>
            </View>
            <View
              style={{
                borderBottomColor: AppColors.BLUE5,
                borderBottomWidth: 1,
                borderStyle: "solid",
                width: "95%"
              }}
            />
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                Intervalo / 1ª Dose:
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  marginLeft: 5,
                  color: AppColors.BLUE5,
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                {(dor.medicamentointervalo||"").length === 0 && (dor.horamedicamento||"").length === 0 && (dor.minutomedicamento||"").length === 0 ? "Indefinido" : (
                  dor.medicamentointervalo+" \n" +
                  dor.horamedicamento + ":" + dor.minutomedicamento
                )}

              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  color: "#fff",
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                Dose:
              </Text>
              <Text
                style={{
                  textAlign: "center",
                  color: AppColors.BLUE5,
                  marginLeft: 5,
                  fontSize: AppMetrics.LG_TEXT
                }}
              >
                {(dor.medicamentoqtd||"").length === 0 && (dor.medicamentounidademedida||"").length === 0 ? "Indefinido" : (
                  dor.medicamentoqtd + " " + dor.medicamentounidademedida
                )}
                
              </Text>
            </View>
            <View
              style={{
                borderBottomColor: AppColors.BLUE5,
                borderBottomWidth: 1,
                borderStyle: "solid",
                width: "95%",
                marginTop: 20
              }}
            />

            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  alignItems: "center",
                  marginTop: AppMetrics.BASE_GRID,
                  borderRightColor: AppColors.BLUE5,
                  borderRightWidth: 1
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 8,
                    color: "#fff",
                    fontWeight: "bold",
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  Horário ou {"\n"}
                  período de {"\n"}
                  início da dor
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 5,
                    color: AppColors.BLUE5,
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  {PERIODO[dor.periodo - 1]}
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 5,
                    color: AppColors.BLUE5,
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  {dor.horaInicio + ":" + dor.minutoInicio}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  flexDirection: "column",
                  marginTop: AppMetrics.BASE_GRID
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 8,
                    color: "#fff",
                    fontWeight: "bold",
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  LOCAL
                </Text>
                <View style={{ flexDirection: "column", marginBottom: 10 }}>
                  <Text
                    style={{
                      textAlign: "center",
                      marginTop: 8,
                      color: AppColors.BLUE5,
                      fontWeight: "bold",
                      fontSize: AppMetrics.MD_TEXT
                    }}
                  >
                    {dor.local != undefined ? dor.local.join("\n") : ""}
                  </Text>
                </View>
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 5,
                    color: AppColors.BLUE5,
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  Observações:
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 5,
                    color: AppColors.BLUE5,
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  {dor.localdorobservacao}
                </Text>
              </View>
            </View>

            <View
              style={{
                borderBottomColor: AppColors.BLUE5,
                borderBottomWidth: 1,
                borderStyle: "solid",
                width: "95%",
                marginTop: 20
              }}
            />

            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  alignItems: "center",
                  marginTop: AppMetrics.BASE_GRID,
                  borderRightColor: AppColors.BLUE5,
                  borderRightWidth: 1
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 8,
                    color: "#fff",
                    fontWeight: "bold",
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  Intensidade
                </Text>
                <Image
                  tintColor={AppColors.BLUE5}
                  source={this.getIconIntensidade(dor.intensidade)}
                  style={{
                    width: 60,
                    height: 60,
                    resizeMode: "contain",
                    marginTop: 10
                  }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  flexDirection: "column",
                  marginTop: AppMetrics.BASE_GRID
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 8,
                    color: "#fff",
                    fontWeight: "bold",
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  Alívio da dor
                </Text>
                <Image
                  tintColor={AppColors.BLUE5}
                  source={this.getIconAlivio(dor.aliviomedicamento)}
                  style={{
                    width: 60,
                    height: 60,
                    resizeMode: "contain",
                    marginTop: 10
                  }}
                />
              </View>
            </View>

            <View
              style={{
                borderBottomColor: AppColors.BLUE5,
                borderBottomWidth: 1,
                borderStyle: "solid",
                width: "95%",
                marginTop: 20
              }}
            />

            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: AppMetrics.BASE_GRID
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  alignItems: "center",
                  padding: 5,
                  marginTop: AppMetrics.BASE_GRID
                }}
              >
                <Text
                  style={{
                    textAlign: "left",
                    alignSelf: "flex-start",
                    marginTop: 8,
                    color: "#fff",
                    fontWeight: "bold",
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  Sintomas:
                </Text>
                <Text
                  style={{
                    textAlign: "left",
                    alignSelf: "flex-start",
                    marginTop: 5,
                    color: AppColors.BLUE5,
                    fontSize: AppMetrics.MD_TEXT
                  }}
                >
                  {dor.caracteristica != undefined
                    ? dor.caracteristica.join("/")
                    : ""}
                  {"|"}
                  {dor.sintomas != undefined ? dor.sintomas.join("/") : ""}
                  {"\n"}
                  {dor.sintomaspresentes != undefined
                    ? dor.sintomaspresentes.join("/")
                    : ""}
                  {"\n"}
                  {dor.iniciosintomas != undefined
                    ? dor.iniciosintomas.join("/")
                    : ""}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  flexDirection: "column",
                  marginTop: AppMetrics.BASE_GRID
                }}
              >
                <TouchableHighlight
                  underlayColor="#00000000"
                  onPress={this.editar}
                  activeOpacity={0.7}
                  style={{ flex: 1, flexDirection: "row" }}
                >
                  <View style={[styles.btn]}>
                    <Text
                      style={{ color: "#fff", fontSize: AppMetrics.LG_TEXT }}
                    >
                      EDITAR
                    </Text>
                  </View>
                </TouchableHighlight>

                <View style={styles.btnDiv}>
                  {this.state.dorAtualIndex == 1 &&
                  this.state.dores.length > 1 ? (
                    <Button rounded onPress={this.prev} style={styles.buttons}>
                      <Image
                        source={require("../assets/arrow_left.png")}
                        style={{ width: 26, height: 26, resizeMode: "contain" }}
                      />
                    </Button>
                  ) : (
                    <View />
                  )}
                  {this.state.dorAtualIndex != this.state.dores.length - 1 ? (
                    <Button rounded onPress={this.next} style={styles.buttons}>
                      <Image
                        source={require("../assets/arrow_right.png")}
                        style={{ width: 26, height: 26, resizeMode: "contain" }}
                      />
                    </Button>
                  ) : (
                    <View />
                  )}
                </View>
              </View>
            </View>
          </View>
        </Dialog>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  },
  btnDiv: {
    marginTop: 10,
    flex: 0.1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  buttons: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    marginLeft: 10,
    width: 54,
    height: 54,
    backgroundColor: AppColors.BLUE,
    elevation: 6
  },
  btn: {
    flex: 1,
    height: 50,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 8,
    paddingRight: 8,
    borderRadius: 10,
    borderWidth: 3,
    justifyContent: "center",
    alignItems: "center",
    borderColor: AppColors.BLUE3,
    backgroundColor: AppColors.BLUE5
  }
});
