import React, { Component } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import Dialog from "./Dialog";
import AppColors from "../styles/AppColors";
import AppMetrics from "../styles/AppMetrics";
import ButtonDial from "./ButtonDial";
import { Image } from "native-base";

export default class DialogAlterarNome extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Dialog
        style={{ borderColor: AppColors.DARK_BLUE }}
        bgColor={AppColors.BLUE4}
        height={385}
        hide={!this.state.editarNome}
        BtnCloseHide={true}
        close={() => {
          this.props.navigation.navigate("Menu");
        }}
        bottons={
          <ButtonDial
            bgColor={AppColors.BLUE4}
            fontColor="#fff"
            text="Concluir"
            onPress={() => {
              this.salvarNome();
            }}
          />
        }
      >
        <View
          style={{ flex: 1, flexDirection: "column", alignItems: "center" }}
        >
          <Text
            style={{
              flex: 1,
              textAlign: "center",
              marginTop: 25,
              color: "#fff",
              fontSize: AppMetrics.XG_TEXT
            }}
          >
            SEJA BEM-VINDO AO
          </Text>

          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              style={{
                flex: 1,
                height: 120,
                resizeMode: "contain",
                marginTop: 40
              }}
              source={require("../assets/logo_white.png")}
            />
          </View>

          <Text
            style={{
              flex: 1,
              textAlign: "center",
              marginTop: 20,
              color: "#fff",
              fontSize: AppMetrics.XG_TEXT
            }}
          >
            Informe seu nome:
          </Text>

          <TextInput
            underlineColorAndroid="transparent"
            value={this.props.nome}
            style={[styles.input, { width: "85%", height: 48, marginTop: 15 }]}
            onChangeText={value => {
              st.nome = value;
              this.setState(st);
            }}
          />
        </View>
      </Dialog>
    );
  }
}
const styles = StyleSheet.create({
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  }
});
