import React, { Component } from "react";
import { View, StyleSheet, Dimensions, ScrollView } from "react-native";
import ButtonDial from "./ButtonDial";
import AppColors from "../styles/AppColors";
import { Container, Content } from "native-base";

var width = Dimensions.get("window").width;
var height = Dimensions.get("window").height;

export default class Dialog extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.close = this.close.bind(this);
  }

  close() {
    if (typeof this.props.close === "function") {
      this.props.close();
    }
  }

  render() {
    if (this.props.hide === true) {
      return null;
    } else {
      return (
        <Container style={[this.styles.container]}>
          <Content style={[this.styles.body, this.props.style]}>
            <ScrollView>{this.props.children}</ScrollView>
          </Content>
          <View style={this.styles.btn}>{this.props.bottons}</View>
        </Container>
      );
    }
  }

  styles = StyleSheet.create({
    container: {
      flex: 1,
      width: width,
      height: height,
      position: "absolute",
      top: 0,
      zIndex: 999,
      paddingTop: width * 0.2,
      backgroundColor: "rgba(0, 0, 0,0.8)",
      alignItems: "center"
    },
    body: {
      flex: 0.8,
      width: width - width * 0.15,
      minHeight: "70%",
      marginTop: 30,
      borderRadius: 10,
      borderWidth: 2,
      top: -50,
      borderColor: this.props.borderColor
        ? this.props.borderColor
        : AppColors.BLUE1,
      opacity: 1,
      backgroundColor: this.props.bgColor
    },
    btn: {
      //width: width - width * 0.15,
      minHeight: height * 0.6,
      marginTop: -40,
      alignItems: "flex-end"
    },
    close: {
      width: width - width * 0.15,
      position: "absolute",
      alignItems: "flex-end",
      marginTop: 30
    }
  });
}
