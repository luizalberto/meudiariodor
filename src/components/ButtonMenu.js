import React, { Component } from "react";
import { View, Text, StyleSheet, Image, ImageBackground, TouchableHighlight } from "react-native";
import AppMetrics from "../styles/AppMetrics";

export default class ButtonMenu extends Component {

  constructor(props){
    super(props);
    this.callFunction = this.callFunction.bind(this);
  }

  componentWillMount(){
    //console.log(this.props);
  }

  callFunction(){
    if(typeof this.props.onPress==="function"){
      this.props.onPress();
    } else{
      alert('Apertou: '+this.props.text);
    }
  }

  getCaveIcon() {
    switch (this.props.icon) {
      case "alerta":
        return require("../assets/icon_alerta.png");
      case "diario":
        return require("../assets/icon_diario.png");
      case "relatorio":
        return require("../assets/icon_relatorio.png");
      case "saiba-mais":
        return require("../assets/icon_saiba_mais.png");
      default:
        return require("../assets/icon_saiba_mais.png");
    }
  }

  getCaveColor() {
    switch (this.props.bgColor) {
      case "blue":
        return require("../assets/btn_cave_blue.png");
      case "red":
        return require("../assets/btn_cave_red.png");
      case "orange":
        return require("../assets/btn_cave_orange.png");
      case "dark-blue":
        return require("../assets/btn_cave_dark_blue.png");
      default:
        return require("../assets/btn_cave.png");
    }
  }

  render() {
    this.props.showIcon = false;
    if (this.props.showIcon) {
      return (
        <TouchableHighlight onPress={this.callFunction} underlayColor="#00000000" activeOpacity={0.7} style={[this.props.style, styles.touchable]}>
        <View style={{ flex: 1 }}>
          <ImageBackground
            style={styles.btn}
            imageStyle={styles.btnImage}
            source={require("../assets/btn_default_shadow.png")}
          >
            <View style={styles.caveContaner}>
              <ImageBackground
                style={styles.cave}
                imageStyle={styles.caveImage}
                source={this.getCaveColor()}
              >
                <Image
                  source={this.getCaveIcon()}
                  style={{ width: 30, height: 30 }}
                />
              </ImageBackground>
            </View>

            <View style={[{paddingRight: 50}, styles.textContainer]}>
              <Text style={styles.text}>
                {this.props.text.toUpperCase()}
              </Text>
            </View>
          </ImageBackground>
        </View>
        </TouchableHighlight>
      );
    } else {
      return (
        <TouchableHighlight onPress={this.callFunction} underlayColor="#00000000" activeOpacity={0.7} style={[this.props.style, styles.touchable]}>
        <View >
          <ImageBackground
            style={styles.btn}
            imageStyle={styles.btnImage}
            source={require("../assets/btn_default_shadow.png")}
          >
            <View style={styles.textContainer}>
              <Text style={styles.text}>
                {this.props.text.toUpperCase()}
              </Text>
            </View>
          </ImageBackground>
        </View>
          </TouchableHighlight>
      );
    }
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  touchable:{
    height: 66,
    width: 280,
    marginBottom: AppMetrics.MD_SPACING,
  },
  btn: {
    height: 66,
    width: 280,
    paddingBottom: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnImage: {},
  caveContaner: {
    flex: 2,
    paddingLeft: 3,
    borderColor: "red"
  },
  cave: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  caveImage: {
    flex: 1,
    margin: 10,
    marginLeft: 9,
    resizeMode: "stretch"
  },
  textContainer: {
    flex: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    alignContent: "center",
    fontSize: 16,
    fontWeight: "bold"
  }
});
