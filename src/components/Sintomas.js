import React, { Component } from 'react';
import { View, Text ,TextInput, StyleSheet} from 'react-native';
import { CheckBox } from 'native-base'
import AppMetrics from '../styles/AppMetrics';
import AppStyles from '../styles/AppStyles';

export default class Sintomas extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data: this.props.state,
    };
    this.updateState = this.updateState.bind(this);
    this.onChange = this.onChange.bind(this);
    this.checked = this.checked.bind(this);
  }

  updateState(st){
    this.props.updateState(st.data);
    this.setState(st);
  }

  onChange(chave, valor){
    var st = this.state;
    var index = st.data.dor[chave].indexOf(valor);
    if(index == -1){
        st.data.dor[chave].push(valor);
    }else{
        st.data.dor[chave].splice(index, 1);
    }
    this.updateState(st)
  }

  checked(chave, valor){
    var st = this.state;
    if(st.data.dor[chave] == undefined){
        st.data.dor[chave] = []
    }
    return st.data.dor[chave].indexOf(valor) != -1;
  }

  render() {
      var st = this.state;
    return (
        <View style={{flex:1, justifyContent:"center", paddingLeft:20}}>
            <Text style={{textAlign: "center", marginTop: 15, color: "#fff", fontSize: AppMetrics.XG_TEXT }}>
                CARACTERÍSTICAS E SINTOMAS
            </Text>
            
            <View style={{flex:1, flexDirection:"column"}}>
                <Text style={{textAlign: "left", marginTop: 15, color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    CARACTERÍSTICAS DA DOR
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Sensação de pontadas/alfinetadas')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{ this.onChange('caracteristica','Sensação de pontadas/alfinetadas')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Sensação de pontadas/alfinetadas
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Cólicas')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('caracteristica','Cólicas')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Cólicas
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Dor em pressão')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('caracteristica','Dor em pressão')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Dor em pressão
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Sensação de choque')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('caracteristica','Sensação de choque')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Sensação de choque
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Dor gelada')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('caracteristica','Dor gelada')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Dor gelada
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Sensação de queimação')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('caracteristica','Sensação de queimação')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Sensação de queimação
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('caracteristica','Formigamento')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('caracteristica','Formigamento')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Formigamento
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Outras:
                </Text>
                <TextInput
                    underlineColorAndroid="transparent"
                    style={[AppStyles.input, { width: '76%' }]}
                    value={st.data.dor.caracteristicasoutros}
                    onChangeText={value => {
                        st.data.dor.caracteristicasoutros = value;
                        this.updateState(st)
                    }}
                />
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />

            <View style={{flex:1, flexDirection:"column", marginTop:20}}>
                <Text style={{textAlign: "left", marginTop: 15, color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    SINTOMAS
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Febre')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Febre')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Febre
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Diarreia')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Diarreia')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Diarreia
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Náuseas')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Náuseas')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Náuseas
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Vômitos')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Vômitos')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Vômitos
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Manchas no corpo')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Manchas no corpo')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Manchas no corpo
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Perda de apetite')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Perda de apetite')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Perda de apetite
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Falta de ar')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Falta de ar')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Falta de ar
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Zumbido no ouvido')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Zumbido no ouvido')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Zumbido no ouvido
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Alterações da visão')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Alterações da visão')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Alterações da visão
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Falta de disposição')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Falta de disposição')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Falta de disposição
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Ansiedade/Estresse')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Ansiedade/Estresse')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Ansiedade/Estresse
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Alterações no ciclo menstrual')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Alterações no ciclo menstrual')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Alterações no ciclo menstrual
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Sonolência')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Sonolência')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Sonolência
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Constipação intestinal')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Constipação intestinal')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Constipação intestinal
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <CheckBox checked={this.checked('sintomas','Sensação de pontadas/alfinetadas')} 
                        style={{marginRight:15, marginLeft:-10}}
                        onPress={()=>{this.onChange('sintomas','Sensação de pontadas/alfinetadas')}}
                />
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Sensação de pontadas/alfinetadas
                </Text>
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%'
                }}
            />
            <View style={{flex:1,flexDirection:"row", marginTop:5, marginBottom:5}}>
                <Text style={{  color: "#fff", fontSize: AppMetrics.SM_TEXT }}>
                    Outros:
                </Text>
                <TextInput
                    underlineColorAndroid="transparent"
                    style={[styles.input, { width: '76%' }]}
                    value={st.data.dor.sintomasoutros}
                    onChangeText={value => {
                    st.data.dor.sintomasoutros = value;
                    this.updateState(st)
                    }}
                />
            </View>
            <View
                style={{
                borderBottomColor: "#fff",
                borderBottomWidth: 1,
                borderStyle: "solid",
                width:'95%',
                marginBottom:20
                }}
            />

        </View>
    );
  }
}

const styles = StyleSheet.create({
    div: {
      flexDirection: "row",
      paddingLeft: 15,
      paddingRight: 15
    },
    input: {
      color: "#000",
      fontSize: 18,
      backgroundColor: "#fff",
      borderRadius: 5,
      marginLeft: 10,
      height: 38,
      paddingTop: 3,
      paddingBottom: 3,
      paddingLeft: 6,
      paddingRight: 6
    },
    text: {
      color: "#fff",
      fontSize: AppMetrics.MD_TEXT
    },
    textSmall:{
        color: "#fff",
        fontSize:AppMetrics.SM_TEXT
    }
  });