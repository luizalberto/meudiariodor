import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  Image
} from "react-native";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import { DatePicker } from "native-base";
import AppStyles from "../styles/AppStyles";

export default class DataDor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.state
    };
    this.selecionaPeriodo = this.selecionaPeriodo.bind(this);
    this.updateStates = this.updateStates.bind(this);
  }

  selecionaPeriodo(periodo) {
    var st = this.state;
    st.data.dor.periodo = periodo;
    this.setState(st);
    this.updateStates();
  }
  updateStates() {
    this.props.updateState(this.state.data);
  }
  render() {
    var st = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Text
          style={{
            textAlign: "center",
            marginTop: 15,
            color: "#fff",
            fontSize: AppMetrics.XG_TEXT
          }}
        >
          DATA E HORA
        </Text>

        <Text
          style={{
            textAlign: "center",
            marginTop: 10,
            color: "#fff",
            fontSize: AppMetrics.SM_TEXT
          }}
        >
          Informe o dia de início da dor:
        </Text>

        <View
          style={[
            styles.div,
            { height: 50, marginTop: 10, justifyContent: "center" }
          ]}
        >
          <TextInput
            underlineColorAndroid="transparent"
            onSubmitEditing={() => {
              this.secondTextInput.focus();
            }}
            style={[AppStyles.input, { width: 45, textAlign:"center" }]}
            keyboardType="numeric"
            returnKeyType="next"
            maxLength={2}
            value={st.data.dor.diaInicio}
            onChangeText={value => {
              st.data.dor.diaInicio = value;
              this.setState(st);
              this.updateStates();
              if (value.length === 2) {
                this.secondTextInput.focus();
              }
            }}
          />
          <Text style={[styles.text, { marginTop: 10 }]}> /</Text>
          <TextInput
            underlineColorAndroid="transparent"
            onSubmitEditing={() => {
              this.thirdTextInput.focus();
            }}
            ref={input => {
              this.secondTextInput = input;
            }}
            style={[AppStyles.input, { width: 45, textAlign:"center" }]}
            keyboardType="numeric"
            returnKeyType="next"
            maxLength={2}
            value={st.data.dor.mesInicio}
            onChangeText={value => {
              st.data.dor.mesInicio = value;
              this.setState(st);
              this.updateStates();
              if (value.length === 2) {
                this.thirdTextInput.focus();
              }
            }}
          />
          <Text style={[styles.text, { marginTop: 8 }]}> /</Text>
          <TextInput
            underlineColorAndroid="transparent"
            ref={input => {
              this.thirdTextInput = input;
            }}
            onSubmitEditing={() => {
              this.fourTextInput.focus();
            }}
            style={[AppStyles.input, { width: 45, textAlign:"center" }]}
            keyboardType="numeric"
            returnKeyType="next"
            maxLength={2}
            value={st.data.dor.anoInicio}
            onChangeText={value => {
              st.data.dor.anoInicio = value;
              this.setState(st);
              this.updateStates();
              if (value.length === 2) {
                this.fourTextInput.focus();
              }
            }}
          />
        </View>
        <Text
          style={{
            textAlign: "center",
            marginTop: 10,
            color: "#fff",
            fontSize: AppMetrics.SM_TEXT
          }}
        >
          Informe a hora de início da dor:
        </Text>
        <View
          style={[
            styles.div,
            { height: 50, marginTop: 10, justifyContent: "center" }
          ]}
        >
          <TextInput
            underlineColorAndroid="transparent"
            style={[AppStyles.input, { width: 45, textAlign:"center" }]}
            ref={input => {
              this.fourTextInput = input;
            }}
            onSubmitEditing={() => {
              this.fiveTextInput.focus();
            }}
            keyboardType="numeric"
            returnKeyType="next"
            maxLength={2}
            value={st.data.dor.horaInicio}
            onChangeText={value => {
              st.data.dor.horaInicio = value;
              this.setState(st);
              this.updateStates();
              if (value.length === 2) {
                this.fiveTextInput.focus();
              }
            }}
          />
          <Text style={[styles.text, { marginTop: 8 }]}> :</Text>
          <TextInput
            underlineColorAndroid="transparent"
            ref={input => {
              this.fiveTextInput = input;
            }}
            style={[AppStyles.input, { width: 45, textAlign:"center" }]}
            keyboardType="numeric"
            returnKeyType="done"
            maxLength={2}
            value={st.data.dor.minutoInicio}
            onChangeText={value => {
              st.data.dor.minutoInicio = value;
              this.setState(st);
              this.updateStates();
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  },
  textSmall: {
    color: "#fff",
    fontSize: AppMetrics.SM_TEXT
  }
});
