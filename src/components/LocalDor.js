import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  ImageBackground
} from "react-native";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import AppStyles from "../styles/AppStyles";

export default class LocalDor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.state
    };
    this.updateState = this.updateState.bind(this);
    this.updateState = this.updateState.bind(this);
    this.checked = this.checked.bind(this);
  }

  updateState(st) {
    this.props.updateState(st.data);
    this.setState(st);
  }

  onChange(chave, valor) {
    var st = this.state;
    var index = st.data.dor[chave].indexOf(valor);
    if (index == -1) {
      st.data.dor[chave].push(valor);
    } else {
      st.data.dor[chave].splice(index, 1);
    }
    this.updateState(st);
  }

  checked(chave, valor) {
    var st = this.state;
    if (st.data.dor[chave] == undefined) {
      st.data.dor[chave] = [];
    }
    return st.data.dor[chave].indexOf(valor) != -1;
  }

  render() {
    var st = this.state;
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          paddingTop: 10,
          paddingBottom: 10,
          flexDirection: "column"
        }}
      >
        <Text
          style={{
            textAlign: "center",
            color: "#fff",
            fontSize: AppMetrics.XG_TEXT
          }}
        >
          LOCAL
        </Text>

        <View
          style={{
            flexDirection: "row",
            marginBottom: 10,
            width: "100%"
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              marginTop: 45
            }}
          >
            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              style={{ marginTop: 0 }}
              onPress={() => {
                this.onChange("local", "Ombro");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  ////textDecorationLine: "underline",
                  marginBottom: 5,
                  color: this.checked("local", "Ombro")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Ombro
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              style={{ marginTop: 0 }}
              onPress={() => {
                this.onChange("local", "Tórax");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 4,
                  ////textDecorationLine: "underline",
                  marginBottom: 5,
                  color: this.checked("local", "Tórax")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Tórax
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Abdômen");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 10,
                  ////textDecorationLine: "underline",
                  marginBottom: 20,
                  color: this.checked("local", "Abdômen")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Abdômen
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Punho");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 0,
                  ////textDecorationLine: "underline",
                  marginBottom: 110,
                  color: this.checked("local", "Punho")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Punho
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Tornozelo");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 0,
                  zIndex: 1,
                  marginLeft: -20,
                  ////textDecorationLine: "underline",
                  width: 100,
                  //marginBottom: 10,
                  color: this.checked("local", "Tornozelo")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Tornozelo
              </Text>
            </TouchableHighlight>
          </View>

          <ImageBackground
            style={[styles.background]}
            resizeMode="stretch"
            source={require("../assets/corpo_frente.png")}
          >
            <View style={{ marginTop: 45, marginLeft: 5 }}>
              <Image
                tintColor={
                  this.checked("local", "Ombro") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 0, marginLeft: 30 }}>
              <Image
                tintColor={
                  this.checked("local", "Tórax") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 8, marginLeft: 30 }}>
              <Image
                tintColor={
                  this.checked("local", "Abdômen") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 18, marginLeft: 0 }}>
              <Image
                tintColor={
                  this.checked("local", "Punho") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: -20, marginLeft: 45 }}>
              <Image
                tintColor={
                  this.checked("local", "Quadril") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 48, marginLeft: 37 }}>
              <Image
                tintColor={
                  this.checked("local", "Joelho") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 37, marginLeft: 29 }}>
              <Image
                tintColor={
                  this.checked("local", "Tornozelo") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
          </ImageBackground>

          <View style={{ flex: 1, flexDirection: "column" }}>
            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              style={{ marginTop: 20 }}
              onPress={() => {
                this.onChange("local", "Coluna cervical");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  ////textDecorationLine: "underline",
                  marginTop: 6,
                  marginBottom: 40,
                  color: this.checked("local", "Coluna cervical")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Coluna cervical
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Coluna lombar");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  //textDecorationLine: "underline",
                  marginBottom: 0,
                  color: this.checked("local", "Coluna lombar")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Coluna lombar
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Quadril");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  //textDecorationLine: "underline",
                  marginBottom: 50,
                  color: this.checked("local", "Quadril")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Quadril
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Joelho");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  //textDecorationLine: "underline",
                  marginBottom: 10,
                  color: this.checked("local", "Joelho")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Joelho
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Panturrilha");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  //textDecorationLine: "underline",
                  marginBottom: 10,
                  color: this.checked("local", "Panturrilha")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Panturrilha
              </Text>
            </TouchableHighlight>
          </View>
          <ImageBackground
            style={[styles.background]}
            resizeMode="stretch"
            source={require("../assets/corpo_costas.png")}
          >
            <View style={{ marginTop: 3, marginLeft: 30 }}>
              <Image
                tintColor={
                  this.checked("local", "Cabeça") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 6, marginLeft: 30 }}>
              <Image
                tintColor={
                  this.checked("local", "Coluna cervical")
                    ? AppColors.BLUE5
                    : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 25, marginLeft: 30 }}>
              <Image
                tintColor={
                  this.checked("local", "Coluna torácica")
                    ? AppColors.BLUE5
                    : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 5, marginLeft: 59 }}>
              <Image
                tintColor={
                  this.checked("local", "Cotovelo") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: -18, marginLeft: 30 }}>
              <Image
                tintColor={
                  this.checked("local", "Coluna lombar")
                    ? AppColors.BLUE5
                    : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 36, marginLeft: 42 }}>
              <Image
                tintColor={
                  this.checked("local", "Coxa") ? AppColors.BLUE5 : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 40, marginLeft: 23 }}>
              <Image
                tintColor={
                  this.checked("local", "Panturrilha")
                    ? AppColors.BLUE5
                    : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
            <View style={{ marginTop: 35, marginLeft: 33 }}>
              <Image
                tintColor={
                  this.checked("local", "Calcanhar/Pé")
                    ? AppColors.BLUE5
                    : "#fff"
                }
                source={require("../assets/marker.png")}
                style={{ resizeMode: "contain", width: 10 }}
              />
            </View>
          </ImageBackground>

          <View style={{ flex: 1, flexDirection: "column" }}>
            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              style={{ marginTop: "10%" }}
              onPress={() => {
                this.onChange("local", "Cabeça");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  //textDecorationLine: "underline",
                  marginBottom: 10,
                  color: this.checked("local", "Cabeça")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Cabeça
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Coluna torácica");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  //textDecorationLine: "underline",
                  marginBottom: 10,
                  color: this.checked("local", "Coluna torácica")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Coluna torácica
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Cotovelo");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  //textDecorationLine: "underline",
                  marginBottom: 60,
                  color: this.checked("local", "Cotovelo")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Cotovelo
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Coxa");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 15,
                  //textDecorationLine: "underline",
                  marginBottom: 60,
                  color: this.checked("local", "Coxa")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Coxa
              </Text>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Calcanhar/Pé");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  width: 70,
                  marginLeft: -15,
                  //textDecorationLine: "underline",
                  marginBottom: 10,
                  color: this.checked("local", "Calcanhar /Pé")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Calcanhar/Pé
              </Text>
            </TouchableHighlight>
          </View>
        </View>

        <View
          style={{
            paddingLeft: 10,
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "#fff",
              marginLeft:10,
              fontSize: AppMetrics.SM_TEXT
            }}
          >
            Observações:
          </Text>

          <TextInput
            underlineColorAndroid="transparent"
            style={[styles.input, { width: "60%" }]}    
            value={st.data.dor.localdorobservacao}
            onChangeText={value => {
              st.data.dor.localdorobservacao = value;
              this.updateState(st);
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
 
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  },
  textSmall: {
    color: "#fff",
    fontSize: AppMetrics.SM_TEXT
  },
  background: {
    width: 70,
    height: 280,
    flexDirection: "column"
  },
  input: {
    color: "#000",
    fontSize: 16,
    backgroundColor: "#fff",
    borderRadius: 5,
  },
});
