import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, TouchableHighlight } from "react-native";
import AppColors from '../styles/AppColors';

export default class ButtonDial extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onPress = this.onPress.bind(this);
  }

  onPress(){
    if(typeof this.props.onPress==="function"){
      this.props.onPress();
    }
  }

  render() {
    return (
      <TouchableHighlight underlayColor="#00000000" onPress={this.onPress} activeOpacity={0.7} style={{ flex: 1 }}>
        <View style={[this.props.style,this.styles.body]}>
            <Text style={{color:this.props.fontColor}}>{this.props.text}</Text>
        </View>
      </TouchableHighlight>
    );
  }

  styles = StyleSheet.create({
    body:{
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 8,
        paddingRight: 8,
        borderRadius: 10,
        backgroundColor: this.props.bgColor,
        
    }
});
}
