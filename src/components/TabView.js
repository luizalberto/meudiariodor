import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  Alert,
  View
} from "react-native";
import ButtonDial from "./ButtonDial";
import AppColors from "../styles/AppColors";
import { Button } from "native-base";

var width = Dimensions.get("window").width;
var height = Dimensions.get("window").height;

export const ICON = {
  data: "data",
  local: "local",
  cabeca: "cabeca",
  intensidade: "intensidade",
  caracteristicas: "caracteristicas",
  medicamento: "medicamento"
};

export default class TabView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabAtiva: 0,
      tabsMax: this.props.tabs.length - 1
    };
    this.getCaveIcon = this.getCaveIcon.bind(this);
    this.prev = this.prev.bind(this);
    this.next = this.next.bind(this);
    this.setTabAtiva = this.setTabAtiva.bind(this);
    this.concluir = this.concluir.bind(this);
  }

  concluir() {
    Alert.alert("Você deseja realmente concluir o registro da dor?",
      "Você poderá consultar ou editar esse registro nos menus [Diário > Histórico] e poderá enviar ao seu médico através do menu [Relatório].",
      [
        {
          text: 'Concluir', onPress: () => {
            if (typeof this.props.concluir === "function") {
              this.props.concluir();
            }
          }
        },
        {
          text: 'Cancelar',
          style: 'cancel',
        },
      ]);
  }

  getCaveIcon(icon) {
    switch (icon) {
      case ICON.data:
        return require("../assets/icon_calendar_hour.png");
      case ICON.local:
        return require("../assets/icon_corpo.png");
      case ICON.cabeca:
        return require("../assets/icon_cabeca.png");
      case ICON.intensidade:
        return require("../assets/icon_pontos.png");
      case ICON.caracteristicas:
        return require("../assets/icon_raio.png");
      case ICON.medicamento:
        return require("../assets/icon_capsula.png");
      default:
        return require("../assets/icon_saiba_mais.png");
    }
  }

  prev() {
    var s = this.state;
    var atual = s.tabAtiva;
    if (atual - 1 >= 0) {
      s.tabAtiva = atual - 1;
      this.setState(s);
    }
  }

  next() {
    var s = this.state;
    var atual = s.tabAtiva;
    if (atual + 1 <= s.tabsMax) {
      s.tabAtiva = atual + 1;
      this.setState(s);
    }
  }

  setTabAtiva(tab) {
    var s = this.state;
    s.tabAtiva = tab;
    this.setState(s);
  }

  render() {
    var tabs = this.props.tabs.map((value, index) => {
      return (
        <TouchableHighlight
          onPress={() => {
            this.setTabAtiva(index);
          }}
          style={[
            this.styles["tab" + (index + 1)],
            index == this.state.tabAtiva
              ? this.styles.tabAtiva
              : this.styles.tab
          ]}
          key={index}
        >
          <View>
            <Image
              resizeMode="contain"
              source={this.getCaveIcon(value.icon)}
              style={{ width: 23, height: 23 }}
            />
          </View>
        </TouchableHighlight>
      );
    });

    var body = this.props.bodys.filter((value, index) => {
      return index == this.state.tabAtiva;
    });

    return (
      <View style={this.styles.container}>
        <View style={this.styles.box}>
          <View style={this.styles.tabContainer}>{tabs}</View>
          <View style={this.styles.tabBody}>
            <View
              style={[
                this.styles.body,
                this.styles["tab" + (this.state.tabAtiva + 1)]
              ]}
            >
              <ScrollView>{body}</ScrollView>
            </View>
          </View>

          <View style={this.styles.btnDiv}>
            <Button rounded onPress={this.prev} style={this.styles.buttons}>
              <Image
                source={require("../assets/arrow_left.png")}
                style={{ width: 26, height: 26, resizeMode: "contain" }}
              />
            </Button>

            {this.state.tabAtiva != this.state.tabsMax ? (
              <Button rounded onPress={this.next} style={this.styles.buttons}>
                <Image
                  source={require("../assets/arrow_right.png")}
                  style={{ width: 26, height: 26, resizeMode: "contain" }}
                />
              </Button>
            ) : (
                <ButtonDial
                  bgColor={AppColors.BLUE}
                  text="Concluir"
                  onPress={this.concluir}
                  fontColor="#fff"
                />
              )}
          </View>
        </View>
      </View>
    );
  }

  styles = StyleSheet.create({
    container: {
      flex: 1,
      position: "absolute",
      flexDirection: "row",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      paddingBottom: 0,
      backgroundColor: "rgba(0, 0, 0,0.5)",
      justifyContent: "center"
    },
    box: {
      marginTop: 0,
      flex: 0.85,
      flexDirection: "column"
    },
    tabContainer: {
      flex: 0.1,
      flexDirection: "row",
      justifyContent: "space-between",
      marginTop: 25,
      backgroundColor: AppColors.BLUE0,
      padding: 5,
      borderTopRightRadius: 10,
      borderTopLeftRadius: 10
    },
    tabBody: {
      flex: 0.75,
      backgroundColor: AppColors.BLUE0,
      flexDirection: "row",
      borderBottomRightRadius: 10,
      borderBottomLeftRadius: 10,
      marginTop: -6,
      paddingLeft: 5,
      paddingRight: 5,
      paddingBottom: 5
    },
    buttons: {
      alignItems: "center",
      justifyContent: "center",
      marginRight: 10,
      marginLeft: 10,
      width: 54,
      height: 54,
      backgroundColor: AppColors.BLUE,
      elevation: 6
    },
    btnDiv: {
      marginTop: 10,
      flex: 0.1,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center"
    },

    tab1: {
      backgroundColor: AppColors.BLUE1
    },
    tab2: {
      backgroundColor: AppColors.BLUE2
    },
    tab3: {
      backgroundColor: AppColors.BLUE3
    },
    tab4: {
      backgroundColor: AppColors.BLUE4
    },
    tab5: {
      backgroundColor: AppColors.BLUE5
    },
    tabAtiva: {
      width: "19%",
      alignItems: "center",
      justifyContent: "center",
      padding: 2,
      borderTopRightRadius: 10,
      borderTopLeftRadius: 10
    },
    tab: {
      width: "18.5%",
      alignItems: "center",
      justifyContent: "center",
      paddingRight: 10,
      paddingLeft: 10,
      marginBottom: 4,
      borderTopRightRadius: 10,
      borderTopLeftRadius: 10
      //marginRight: 5
    },
    body: {
      flex: 1,
      backgroundColor: "#bbb"
    }
  });
}
