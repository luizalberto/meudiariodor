import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  ImageBackground
} from "react-native";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import Canvas, { Image as CanvasImage } from "react-native-canvas";

export default class CabecaDor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.state,
      uri: "",
      uriMarker: ""
    };
    this.updateState = this.updateState.bind(this);
    this.checked = this.checked.bind(this);
  }

  updateState(st) {
    this.props.updateState(st.data);
    this.setState(st);
  }

  handleCanvas = canvas => {
    const image = new CanvasImage(canvas);

    canvas.width = 100;
    canvas.height = 100;
    const ctx = canvas.getContext("2d");
    image.addEventListener("load", () => {
      ctx.drawImage(image, 0, 0, 100, 100);
    });
  };

  onChange(chave, valor) {
    var st = this.state;
    var index = st.data.dor[chave].indexOf(valor);
    if (index == -1) {
      st.data.dor[chave].push(valor);
    } else {
      st.data.dor[chave].splice(index, 1);
    }
    this.updateState(st);
  }

  checked(chave, valor) {
    var st = this.state;
    if (st.data.dor[chave] == undefined) {
      st.data.dor[chave] = [];
    }
    return st.data.dor[chave].indexOf(valor) != -1;
  }

  render() {
    var st = this.state;
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Text
          style={{
            textAlign: "center",
            marginTop: 15,
            color: "#fff",
            fontSize: AppMetrics.XG_TEXT
          }}
        >
          LOCAL
        </Text>

        <View style={{ flex: 1, padding: 10, alignItems: "center" }}>
          <TouchableHighlight
            underlayColor="#00000000"
            activeOpacity={0.7}
            onPress={() => {
              this.onChange("local", "Em toda a cabeça");
            }}
          >
            <Text
              style={{
                textAlign: "center",
                marginTop: 5,
                marginBottom: 10,
                color: this.checked("local", "Em toda a cabeça")
                  ? AppColors.BLUE5
                  : "#fff",
                fontSize: 10
              }}
            >
              Em toda a cabeça
            </Text>
          </TouchableHighlight>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              onPress={() => {
                this.onChange("local", "Frontal");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 5,
                  marginBottom: 10,
                  color: this.checked("local", "Frontal")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Frontal
              </Text>
            </TouchableHighlight>

            <ImageBackground
              style={[styles.background]}
              resizeMode="stretch"
              source={require("../assets/icon_cabeca.png")}
            >
              <View style={{ marginTop: -60, marginLeft: 0 }}>
                <Image
                  tintColor={
                    this.checked("local", "Em toda a cabeça")
                      ? AppColors.BLUE5
                      : "#fff"
                  }
                  source={require("../assets/marker.png")}
                  style={{ resizeMode: "contain", width: 15 }}
                />
              </View>

              <View style={{ marginTop: 0, marginLeft: 0 }}>
                <Image
                  tintColor={
                    this.checked("local", "Frontal") ? AppColors.BLUE5 : "#fff"
                  }
                  source={require("../assets/marker.png")}
                  style={{ resizeMode: "contain", width: 15 }}
                />
              </View>
            </ImageBackground>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              style={{ flex: 1, flexDirection: "column-reverse" }}
              onPress={() => {
                this.onChange("local", "Na nuca");
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: -50,
                  marginBottom: 10,
                  color: this.checked("local", "Na nuca")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10,
                  width:30

                }}
              >
                Na nuca
              </Text>
            </TouchableHighlight>
            <ImageBackground
              style={[styles.background]}
              resizeMode="stretch"
              source={require("../assets/icon_cabeca_tras.png")}
            >
              <View style={{ marginTop: 0, marginLeft: 0 }}>
                <Image
                  tintColor={
                    this.checked("local", "Na nuca") ? AppColors.BLUE5 : "#fff"
                  }
                  source={require("../assets/marker.png")}
                  style={{ resizeMode: "contain", width: 15 }}
                />
              </View>
            </ImageBackground>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: "row",
              marginTop: 30,
              marginBottom: 10,
              marginLeft: "28%"
            }}
          >
            <ImageBackground
              style={[styles.background]}
              resizeMode="stretch"
              source={require("../assets/icon_cabeca_lado.png")}
            >
              <View style={{ marginTop: 0, marginLeft: 0 }}>
                <Image
                  tintColor={
                    this.checked("local", "Em um lado da cabeça")
                      ? AppColors.BLUE5
                      : "#fff"
                  }
                  source={require("../assets/marker.png")}
                  style={{ resizeMode: "contain", width: 15 }}
                />
              </View>
            </ImageBackground>

            <TouchableHighlight
              underlayColor="#00000000"
              activeOpacity={0.7}
              style={{ flex: 1, flexDirection: "column-reverse" }}
              onPress={() => {
                this.onChange("local", "Em um lado da cabeça");
              }}
            >
              <Text
                style={{
                  textAlign: "left",
                  marginTop: 5,
                  marginBottom: 10,
                  color: this.checked("local", "Em um lado da cabeça")
                    ? AppColors.BLUE5
                    : "#fff",
                  fontSize: 10
                }}
              >
                Em um lado da cabeça
              </Text>
            </TouchableHighlight>
          </View>
        </View>

        <View
          style={{
            paddingLeft: 10,
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              textAlign: "center",
              color: "#fff",
              fontSize: AppMetrics.SM_TEXT
            }}
          >
            Observações:
          </Text>

          <TextInput
            multiline={true}
            underlineColorAndroid="transparent"
            style={[styles.input, { width: "60%" }]}
            value={st.data.dor.localdorobservacao}
            onChangeText={value => {
              st.data.dor.localdorobservacao = value;
              this.updateState(st);
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 22,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  },
  textSmall: {
    color: "#fff",
    fontSize: AppMetrics.SM_TEXT
  },
  background: {
    width: 100,
    height: 100,
    padding: -50,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  }
});
