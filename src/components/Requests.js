import AppValues from "./AppValues";

const apiInsert = AppValues.BASE_URL+"index.php/report/save_data";

export async function saveReport(params) {
  console.log("PARAMS ====================================================== ");
  console.log(params);
  try {
    let response = await fetch(apiInsert, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    });

    let json = await response.json();
    console.log(json)
    return json;
  } catch (error) {
    console.log("Um erro aconteceu!", error);
    return null;
  }
}
