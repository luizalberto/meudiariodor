import React, { Component } from "react";
import { View, Text, StyleSheet, Image, ImageBackground, TouchableHighlight } from "react-native";
import AppMetrics from "../styles/AppMetrics";

export default class ButtonCustom extends Component {

  constructor(props){
    super(props);
    this.callFunction = this.callFunction.bind(this);
  }

  callFunction(){
    if(typeof this.props.onPress==="function"){
      this.props.onPress();
    } else{
      alert('Apertou: '+this.props.text);
    }
  }

  getBackgroud() {
    switch (this.props.bgColor) {
      case "blue":
        return require("../assets/btn_diario.png");
      case "orange":
        return require("../assets/btn_saiba_mais.png");
      case "red":
        return require("../assets/btn_lembrete.png");
      case "dark-blue":
        return require("../assets/btn_relatorio.png");
      default:
        return require("../assets/btn_relatorio.png");
    }
  }

  render() {
      return (
        <TouchableHighlight onPress={this.callFunction} underlayColor="#00000000" activeOpacity={0.7} style={[this.props.style, styles.touchable]}>
        <View style={{ flex: 1 }}>
          <ImageBackground
            style={styles.btn}
            imageStyle={styles.btnImage}
            source={this.getBackgroud()}
          >
            <View style={[{paddingRight: 50}, styles.textContainer]}>
              <Text style={styles.text}>
                {this.props.text.toUpperCase()}
              </Text>
            </View>
          </ImageBackground>
        </View>
        </TouchableHighlight>
      );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  touchable:{
    height: 66,
    width: 280,
    marginBottom: AppMetrics.MD_SPACING,
  },
  btn: {
    height: 66,
    width: 280,
    paddingBottom: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  btnImage: {},
  textContainer: {
    flex: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    paddingLeft: 80,
    alignContent: "center",
    fontSize: 16,
    fontWeight: "bold"
  }
});
