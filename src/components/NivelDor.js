import React, { Component } from "react";
import { View, Text, TouchableHighlight, Image } from "react-native";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
export default class NivelDor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.state,
      intensidade: this.props.state.dor.intensidade
    };
    this.updateState = this.updateState.bind(this);
    this.selecionaIntensidade = this.selecionaIntensidade.bind(this);
  }

  selecionaIntensidade(intensidade) {
    var st = this.state;
    st.intensidade = intensidade;
    st.data.dor.intensidade = intensidade;
    this.updateState(st);
  }

  updateState(st) {
    this.props.updateState(st.data);
    this.setState(st);
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Text
          style={{
            textAlign: "center",
            marginTop: 15,
            color: "#fff",
            fontSize: AppMetrics.XG_TEXT
          }}
        >
          INTENSIDADE
        </Text>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            marginBottom: 15,
            justifyContent: "center"
          }}
        >
          <View
            style={{
              justifyContent: "flex-end",
              paddingTop: 8,
              paddingBottom: 8
            }}
          >
            <Image
              source={require("../assets/regua.png")}
              style={{ height: 330, resizeMode: "contain" }}
            />
          </View>
          <View
            style={{
              width: 80,
              flexDirection: "column",
              paddingTop: 10,
              paddingBottom: 10,
              alignItems: "center"
            }}
          >
            <TouchableHighlight
              underlayColor="#00000000"
              onPress={this.onPress}
              activeOpacity={0.7}
              style={{ flex: 1, marginTop: 5 }}
              onPress={() => {
                this.selecionaIntensidade(1);
              }}
            >
              <Image
                tintColor={
                  this.state.intensidade == 1 ? AppColors.BLUE4 : "#fff"
                }
                source={require("../assets/smile1.png")}
                style={{
                  flex: 1,
                  height: 50,
                  width: 50,
                  resizeMode: "contain"
                }}
              />
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              onPress={this.onPress}
              activeOpacity={0.7}
              style={{ flex: 1, marginTop: 5 }}
              onPress={() => {
                this.selecionaIntensidade(2);
              }}
            >
              <Image
                tintColor={
                  this.state.intensidade == 2 ? AppColors.BLUE4 : "#fff"
                }
                source={require("../assets/smile2.png")}
                style={{
                  flex: 1,
                  height: 50,
                  width: 50,
                  resizeMode: "contain"
                }}
              />
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              onPress={this.onPress}
              activeOpacity={0.7}
              style={{ flex: 1, marginTop: 5 }}
              onPress={() => {
                this.selecionaIntensidade(3);
              }}
            >
              <Image
                tintColor={
                  this.state.intensidade == 3 ? AppColors.BLUE4 : "#fff"
                }
                source={require("../assets/smile3.png")}
                style={{
                  flex: 1,
                  height: 50,
                  width: 50,
                  resizeMode: "contain"
                }}
              />
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              onPress={this.onPress}
              activeOpacity={0.7}
              style={{ flex: 1, marginTop: 5 }}
              onPress={() => {
                this.selecionaIntensidade(4);
              }}
            >
              <Image
                tintColor={
                  this.state.intensidade == 4 ? AppColors.BLUE4 : "#fff"
                }
                source={require("../assets/smile4.png")}
                style={{
                  flex: 1,
                  height: 50,
                  width: 50,
                  resizeMode: "contain"
                }}
              />
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor="#00000000"
              onPress={this.onPress}
              activeOpacity={0.7}
              style={{ flex: 1, marginTop: 5 }}
              onPress={() => {
                this.selecionaIntensidade(5);
              }}
            >
              <Image
                tintColor={
                  this.state.intensidade == 5 ? AppColors.BLUE4 : "#fff"
                }
                source={require("../assets/smile5.png")}
                style={{
                  flex: 1,
                  height: 50,
                  width: 50,
                  resizeMode: "contain"
                }}
              />
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
}
