import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  StyleSheet,
  AsyncStorage
} from "react-native";
import { LocaleConfig, Calendar } from "react-native-calendars";
import AppStyles from "../styles/AppStyles";
import Dialog from "../components/Dialog";
import AppColors from "../styles/AppColors";
import AppMetrics from "../styles/AppMetrics";

LocaleConfig.locales["br"] = {
  monthNames: [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
  ],
  monthNamesShort: [
    "Jan.",
    "Fev.",
    "Mar",
    "Abril",
    "Maio",
    "Jun",
    "Jul.",
    "Ago",
    "Set.",
    "Out.",
    "Nov.",
    "Dec."
  ],
  dayNames: [
    "Domingo",
    "Segunda",
    "Terça",
    "Quarta",
    "Quinta",
    "Sexta",
    "Sábado"
  ],
  dayNamesShort: ["Dom.", "Seg.", "Ter.", "Qua.", "Qui.", "Sex.", "Sab."]
};
LocaleConfig.defaultLocale = "br";

const KEY_DOR = "DOR";
export default class ScreenHitorico extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Histórico"
  });

  constructor(props) {
    super(props);
    var data = new Date();
    var mes =
      (data.getMonth() + 1).toString().length == 1
        ? "0" + (data.getMonth() + 1)
        : (data.getMonth() + 1).toString();
    var ano = data
      .getFullYear()
      .toString()
      .substring(2, 4);
    this.state = {
      mes: mes,
      ano: ano,
      dores: [],
      markers: {},
      dataAtual: ""
    };
    this.listar = this.listar.bind(this);
    this.createMarkerDate = this.createMarkerDate.bind(this);
    this.setDate = this.setDate.bind(this);
    this.getDoresByData = this.getDoresByData.bind(this);
  }

  componentDidMount() {
    this.listar();
    this.setDate();
  }

  setDate() {
    var st = this.state;
    if (st.mes.length == 2 && st.ano.length == 2) {
      var dt = new Date();
      st.dataAtual = "20" + st.ano + "-" + st.mes + "-01";
      this.setState(st);
    }
  }

  createMarkerDate(dores) {
    var s = this.state;
    var markers = {};
    console.log("Dores",dores);
    dores.map(dor => {
      var key =
      "20" + dor.anoInicio + "-" + dor.mesInicio + "-" + dor.diaInicio;
      markers[key] = { selected: true, marked: true, selectedColor: "#213f61" };
    });
    return markers;
  }

  getDoresByData(dia, mes, ano) {
    var dores = this.state.dores;
    var lista = dores.filter(dor => {
      return (
        "20" + dor.anoInicio == ano &&
        dor.mesInicio == mes &&
        dor.diaInicio == dia
      );
    });
    return lista;
  }

  async listar() {
    var s = this.state;
    await AsyncStorage.getAllKeys((err, keys) => {
      var rs = keys.filter(key => {
        return key.indexOf(KEY_DOR) != -1;
      });
      AsyncStorage.multiGet(rs, (err, itens) => {
        dores = itens.map(item => {
          let dor = JSON.parse(item[1]);
          dor.id = item[0];
          return dor;
        });
        s.dores = dores;
        var mk = this.createMarkerDate(dores);
        s.markers = mk;
        this.setState(s);
      });
    });
  }

  render() {
    var st = this.state;
    return (
      <ImageBackground
        style={AppStyles.container}
        source={require("../assets/app_bg.png")}
      >
        <Dialog
          style={{ borderColor: AppColors.BLUE3 }}
          bgColor={AppColors.BLUE2}
          height={"80%"}
          close={() => {
            this.props.navigation.navigate("Diario");
          }}
        >
          <Text
            style={{
              textAlign: "center",
              marginTop: 25,
              color: "#fff",
              fontSize: AppMetrics.XG_TEXT
            }}
          >
            HISTÓRICO
          </Text>
          <View
            style={[
              styles.div,
              { height: 25, marginTop: 20, justifyContent: "center" }
            ]}
          >
            <TextInput
              onSubmitEditing={() => {
                this.secondTextInput.focus();
              }}
              keyboardType="numeric"
              returnKeyType="next"
              underlineColorAndroid="transparent"
              style={[styles.input, { width: 40 }]}
              maxLength={2}
              value={st.mes}
              onChangeText={value => {
                st.mes = value;
                this.setState(st);
                this.setDate();
                if (value.length === 2) {
                  this.secondTextInput.focus();
                }
              }}
            />
            <Text style={[styles.text]}> /</Text>
            <TextInput
              underlineColorAndroid="transparent"
              style={[styles.input, { width: 40 }]}
              ref={input => {
                this.secondTextInput = input;
              }}
              keyboardType="numeric"
              returnKeyType="done"
              maxLength={2}
              value={st.ano}
              onChangeText={value => {
                st.ano = value;
                this.setState(st);
                this.setDate();
              }}
            />
          </View>
          <View
            style={[
              styles.div,
              { height: 25, marginTop: 5, justifyContent: "center" }
            ]}
          >
            <Text style={[styles.text]}>MÊS / ANO</Text>
          </View>

          <View style={{ marginTop: 20, padding: 15, marginBottom: 20 }}>
            <Calendar
              hideArrows={true}
              current={st.dataAtual}
              markedDates={this.state.markers}
              onDayPress={data => {
                var dores = this.getDoresByData(
                  data.day,
                  data.month,
                  data.year
                );
                if (dores.length > 0) {
                  this.props.navigation.navigate("Resumo", {
                    dores: dores
                  });
                }
              }}
            />
          </View>
        </Dialog>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  calendar: {},
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  }
});
