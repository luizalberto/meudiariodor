import React, { Component } from "react";
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  StatusBar
} from "react-native";
import AppValues from "../components/AppValues";

export default class Splash extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Entrada",
    header: null
  });

  constructor(props) {
    super(props);
    this.callMainScreen = this.callMainScreen.bind(this);
  }

  componentWillMount() {
    this.callMainScreen();
  }

  callMainScreen() {
    //const { push } = this.props.navigation.push;
    setTimeout(() => this.props.navigation.replace("Iniciativa"), AppValues.FREEZE_TIME);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden={true} />
        <ImageBackground
          style={styles.background}
          source={require("../assets/app_bg.png")}
        >
          <View style={[{ flex: 1 }, styles.borderTeste]} />

          <Image
            style={[styles.logoDiario, styles.borderTeste]}
            source={require("../assets/logo.png")}
          />
          <View style={[styles.containerBotoes, styles.borderTeste]}>
            <Image
              style={[styles.botoes, styles.borderTeste]}
              source={require("../assets/btn_calendar.png")}
            />
            <Image
              style={[styles.botoes, styles.borderTeste]}
              source={require("../assets/btn_interroga.png")}
            />
            <Image
              style={[styles.botoes, styles.borderTeste]}
              source={require("../assets/btn_alert.png")}
            />
            <Image
              style={[styles.botoes, styles.borderTeste]}
              source={require("../assets/btn_esteto.png")}
            />
          </View>
          <Image
            style={[styles.logoCristalia, styles.borderTeste]}
            source={require("../assets/logo_cristalia.png")}
          />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  borderTeste: {
    borderColor: "red"
  },
  background: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  logoDiario: {
    flex: 2,
    width: 300,
    height: 265,
    resizeMode: "contain"
  },
  containerBotoes: {
    flex: 1,
    paddingTop: 30,
    width: "80%",
    flexDirection: "row",
    alignItems: "center"
  },
  botoes: {
    flex: 1,
    height: 60,
    resizeMode: "contain"
  },
  logoCristalia: {
    flex: 1,
    width: 160,
    resizeMode: "contain"
  }
});
