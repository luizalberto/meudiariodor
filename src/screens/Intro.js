import React, { Component } from "react";
import { StyleSheet, Image, Dimensions } from "react-native";
import { View, Text, Icon, Container } from "native-base";
import Menu from "./Menu";

import AppColors from "../styles/AppColors";
import AppIntroSlider from "../components/AppIntroSlider";

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: "rgba(0, 0, 0, .2)",
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width:250,
    height:250,
    borderRadius: 5,
    borderColor: AppColors.DARK,
    borderWidth: 1
  },
  mainContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    color: AppColors.DARK,
    backgroundColor: "transparent",
    textAlign: "center",
    paddingHorizontal: 16
  },
  title: {
    fontSize: 22,
    color: AppColors.DARK,
    backgroundColor: "transparent",
    textAlign: "center",
    marginTop: 16
  }
});

const slides = [
  {
    key: "slide1",
    titleStyle: styles.title,
    textStyle: styles.text,
    title: "Como funciona",
    text:
      "Com o aplicativo Meu Diário Dor, você poderá, por meio do seu tablet ou smatphone, anotar seus sintomas, a intensidade das dores, o local em que elas ocorrem e a frequência que aparecem.",
    image: require("../assets/slides/slide-menu.png"),
    imageStyle: styles.image,
    backgroundColor: AppColors.BLUE0
  },
  {
    key: "slide2",
    title: "Diário",
    titleStyle: styles.title,
    textStyle: styles.text,
    text:
      "Local atribuído para fazer o registro da dor. Aqui você pode preencher o dia e a hora da sua dor, assim como a intensidade, região do corpo, características e o remédio escolhido para aliviar o incômodo.",
    image: require("../assets/slides/slide-registro2.png"),
    imageStyle: styles.image,
    backgroundColor: "#57b3e3"
  },
  {
    key: "slide3",
    titleStyle: styles.title,
    textStyle: styles.text,
    title: "Saiba Mais",
    text:
      "Antes de entrar em contato com o médico, você também pode esclarecer as suas dúvidas sobre os tipos de dores com informações completas e referenciadas.",
    image: require("../assets/slides/slide-saibamais.png"),
    imageStyle: styles.image,
    backgroundColor: "#f3c342"
  },
  {
    key: "slide4",
    titleStyle: { color: "#ffffff" },
    textStyle: { color: "#ffffff" },
    title: "Lembrete",
    text:
      "Como um verdadeiro alarme, esta opção tem como objetivo informar os horários para fazer uso do medicamento escolhido.",
    image: require("../assets/slides/slide-lembrete.png"),
    imageStyle: styles.image,
    backgroundColor: "#820f05"
  },
  {
    key: "slide5",
    titleStyle: { color: "#ffffff" },
    textStyle: { color: "#ffffff" },
    title: "Relatório Médico",
    text:
      "Área destinada para enviar o seu relatório ao médico, seja por e-mail ou compartilhando em qualquer aplicativo de mensagem instantânea.",
    image: require("../assets/slides/slide-relatorio.png"),
    imageStyle: styles.image,
    backgroundColor: "#113e53"
  }
];

export default class Intro extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Intro",
    header: null
  });

  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false
    };
  }

  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    this.setState({ showRealApp: true });
  };

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" style={{ color: "#ffffff" }} />
      </View>
    );
  };

  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="checkmark" color="white" style={{ color: "#ffffff" }} />
      </View>
    );
  };

  render() {
    if (this.state.showRealApp) {
      return this.props.navigation.replace("Menu");
    } else {
      return (
        <AppIntroSlider
          slides={slides}
          renderDoneButton={this._renderDoneButton}
          renderNextButton={this._renderNextButton}
          onDone={this._onDone}
        />
      );
    }
  }
}
