import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  StyleSheet,
  AsyncStorage
} from "react-native";
import Dialog from "../components/Dialog";
import AppStyles from "../styles/AppStyles";
import ButtonDial from "../components/ButtonDial";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import { saveReport } from "../components/Requests";

const KEY_DOR = "DOR";
export default class ScreenRelatorio extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Relatório médico",
    headerStyle: {
      backgroundColor: AppColors.DARK_BLUE
    }
  });

  constructor(props) {
    super(props);
    var data = new Date();
    var dia = data.getDate().toString().length == 1 ? "0" + data.getDate() : data.getDate().toString();
    var mes = (data.getMonth() + 1).toString().length == 1 ? "0" + (data.getMonth() + 1) : (data.getMonth() + 1).toString();
    var ano = data.getFullYear().toString().substring(2, 4);

    this.state = {
      medico: "",
      relatorio: {
        diaInicio: (parseInt(dia) - 1).toString(),
        mesInicio: mes,
        anoInicio: ano,
        diaFim: dia,
        mesFim: mes,
        anoFim: ano
      }
    };

    this.enviar = this.enviar.bind(this);
  }

  async enviar() {
    var s = this.state;

    await AsyncStorage.getAllKeys((err, keys) => {
      var rs = keys.filter(key => {
        return key.indexOf(KEY_DOR) != -1; // TODO add filter da data
      });
      AsyncStorage.multiGet(rs, (err, itens) => {
        dores = itens.map(item => {
          let med = JSON.parse(item[1]);
          med.id = item[0];
          return med;
        }).filter(obj => {
          var dia =
            obj.diaInicio.length == 1 ? "0" + obj.diaInicio : obj.diaInicio;
          var mes =
            obj.mesInicio.length == 1 ? "0" + obj.mesInicio : obj.mesInicio;
          var data = new Date(
            "20" + obj.anoInicio + "/" + mes + "/" + dia + " 00:00:00"
          );

          var diaInicio =
            s.relatorio.diaInicio.length == 1
              ? "0" + s.relatorio.diaInicio
              : s.relatorio.diaInicio;
          var mesInicio =
            s.relatorio.mesInicio.length == 1
              ? "0" + s.relatorio.mesInicio
              : s.relatorio.mesInicio;
          var dataInicio = new Date(
            "20" +
            s.relatorio.anoInicio +
            "/" +
            mesInicio +
            "/" +
            diaInicio +
            " 00:00:00"
          );

          var diaFim =
            s.relatorio.diaFim.length == 1
              ? "0" + s.relatorio.diaFim
              : s.relatorio.diaFim;
          var mesFim =
            s.relatorio.mesFim.length == 1
              ? "0" + s.relatorio.mesFim
              : s.relatorio.mesFim;
          var dataFim = new Date(
            "20" +
            s.relatorio.anoInicio +
            "/" +
            mesFim +
            "/" +
            diaFim +
            " 00:00:00"
          );

          return data >= dataInicio && data <= dataFim;
        });
        //let usuario = AsyncStorage.getItem("usuario");
        var dados = this.state.relatorio;
        dados.dores = dores;
        //dados.usuario = usuario;
        dados.medico = this.state.medico;
        let result = this.gerarRelatorio(dados)

        result.then(json => {
          this.props.navigation.navigate("Pdf", { dados: json });
        })
      });
    });
  }


  async gerarRelatorio(dores) {
    let paciente = await AsyncStorage.getItem("usuario");

    const newReport = {
      medico: dores.medico,
      paciente: paciente,
      json: dores
    };

    return await saveReport(newReport);
  }

  render() {
    var st = this.state;
    return (
      <ImageBackground
        style={AppStyles.container}
        source={require("../assets/app_bg.png")}
      >
        <Dialog
          style={{ borderColor: AppColors.DARK_BLUE }}
          bgColor={AppColors.BLUE4}
          height={"60%"}
          close={() => {
            this.props.navigation.navigate("Menu");
          }}
          bottons={
            <ButtonDial
              bgColor={AppColors.BLUE4}
              fontColor="#fff"
              text="VISUALIZAR RELATÓRIO"
              onPress={this.enviar}
            />
          }
        >
          <Text
            style={{
              textAlign: "center",
              marginTop: 25,
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            Informe o nome do médico:
          </Text>
          <View style={[styles.div, { marginTop: 10, alignItems: "center" }]}>
            <TextInput
              onSubmitEditing={() => {
                this.fourTextInput.focus();
              }}
              returnKeyType="next"
              underlineColorAndroid="transparent"
              value={st.medico}
              style={[
                styles.input,
                { width: "95%", textAlign: "center", height: 42 }
              ]}
              onChangeText={value => {
                st.medico = value;
                this.setState(st);
              }}
            />
          </View>

          <Text
            style={{
              textAlign: "center",
              marginTop: 25,
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            Período do relatório:
          </Text>

          <View
            style={[styles.div, { marginTop: 15, justifyContent: "center" }]}
          >
            <TextInput
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                { width: 45, marginTop: -10, textAlign: "center", height: 42 }
              ]}
              ref={input => {
                this.fourTextInput = input;
              }}
              onSubmitEditing={() => {
                this.fiveTextInput.focus();
              }}
              keyboardType="numeric"
              returnKeyType="next"
              maxLength={2}
              value={st.relatorio.diaInicio}
              onChangeText={value => {
                st.relatorio.diaInicio = value;
                this.setState(st);
                if (value.length === 2) {
                  this.fiveTextInput.focus();
                }
              }}
            />
            <Text style={[styles.text]}> /</Text>
            <TextInput
              ref={input => {
                this.fiveTextInput = input;
              }}
              onSubmitEditing={() => {
                this.sixTextInput.focus();
              }}
              keyboardType="numeric"
              returnKeyType="next"
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                { width: 45, marginTop: -10, textAlign: "center", height: 42 }
              ]}
              maxLength={2}
              value={st.relatorio.mesInicio}
              onChangeText={value => {
                st.relatorio.mesInicio = value;
                this.setState(st);
                if (value.length === 2) {
                  this.sixTextInput.focus();
                }
              }}
            />
            <Text style={[styles.text]}> /</Text>
            <TextInput
              ref={input => {
                this.sixTextInput = input;
              }}
              onSubmitEditing={() => {
                this.sevenTextInput.focus();
              }}
              keyboardType="numeric"
              returnKeyType="next"
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                { width: 45, marginTop: -10, textAlign: "center", height: 42 }
              ]}
              maxLength={2}
              value={st.relatorio.anoInicio}
              onChangeText={value => {
                st.relatorio.anoInicio = value;
                this.setState(st);
                if (value.length === 2) {
                  this.sevenTextInput.focus();
                }
              }}
            />
          </View>

          <Text
            style={{
              textAlign: "center",
              marginTop: 15,
              color: "#fff",
              fontSize: AppMetrics.MD_TEXT
            }}
          >
            a
          </Text>
          <View
            style={[styles.div, { marginTop: 15, justifyContent: "center" }]}
          >
            <TextInput
              ref={input => {
                this.sevenTextInput = input;
              }}
              onSubmitEditing={() => {
                this.eightTextInput.focus();
              }}
              keyboardType="numeric"
              returnKeyType="next"
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                { width: 45, marginTop: -10, textAlign: "center", height: 42 }
              ]}
              maxLength={2}
              value={st.relatorio.diaFim}
              onChangeText={value => {
                st.relatorio.diaFim = value;
                this.setState(st);
                if (value.length === 2) {
                  this.eightTextInput.focus();
                }
              }}
            />
            <Text style={[styles.text]}> /</Text>
            <TextInput
              ref={input => {
                this.eightTextInput = input;
              }}
              onSubmitEditing={() => {
                this.nineTextInput.focus();
              }}
              keyboardType="numeric"
              returnKeyType="next"
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                { width: 45, marginTop: -10, textAlign: "center", height: 42 }
              ]}
              maxLength={2}
              value={st.relatorio.mesFim}
              onChangeText={value => {
                st.relatorio.mesFim = value;
                this.setState(st);
                if (value.length === 2) {
                  this.nineTextInput.focus();
                }
              }}
            />
            <Text style={[styles.text]}> /</Text>
            <TextInput
              ref={input => {
                this.nineTextInput = input;
              }}
              keyboardType="numeric"
              returnKeyType="done"
              underlineColorAndroid="transparent"
              style={[
                styles.input,
                { width: 45, marginTop: -10, textAlign: "center", height: 42 }
              ]}
              maxLength={2}
              value={st.relatorio.anoFim}
              onChangeText={value => {
                st.relatorio.anoFim = value;
                this.setState(st);
              }}
            />
          </View>
        </Dialog>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  }
});
