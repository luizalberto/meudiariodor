import { View } from "native-base";
import React, { Component } from "react";
import { Button, Dimensions, Share, StyleSheet } from "react-native";
import Pdf from "react-native-pdf";
import AppValues from "../components/AppValues";
import AppColors from "../styles/AppColors";

export default class PdfScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Visualizar relatório",
    headerStyle: {
      backgroundColor: AppColors.DARK_BLUE
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      dados: {}
    };
  }

  share() {
    let params = this.props.navigation.getParam("dados", null);
    console.log("params ::::::::::::::::::::::::::::::::");

    Share.share({
      title: "Meu Diario Dor - Relatório",
      message:
        "Para acessar o meu relatório clique no link a seguir: " +
        AppValues.BASE_URL + "index.php/report/view/" + params.cod + "\nUtilize a senha: " + params.pass
    });
  };

  render() {
    let dados = this.props.navigation.getParam("dados");
    const source = {uri:AppValues.BASE_URL + "index.php/report/view/" + dados.cod + "/0", cache:true};
        
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 30,
            width: "100%",
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
            alignSelf: "flex-end",
            alignItems: "flex-end"
          }}
        >
          <Button
            title={"Voltar"}
            onPress={()=>this.props.navigation.navigate("Relatorio")}
            style={{ flex: 1, borderRadius: 8 }}
          />

          <Button
            title={"Compartilhar"}
            onPress={() => this.share()}
            style={{
              flex: 1,
              borderRadius: 10,
              backgroundColor: AppColors.ORANGE
            }}
          />
        </View>

        <Pdf source={source} style={styles.pdf} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 25
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width
  }
});
