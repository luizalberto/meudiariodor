import { Button, Container, Icon } from "native-base";
import React, { Component } from "react";
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View
} from "react-native";
import DORES from "../components/Dores";
import AppColors from "../styles/AppColors";
import AppMetrics from "../styles/AppMetrics";

export default class ScreenSaibaMais extends Component {
  static navigationOptions = ({ navigation, navigationOptions }) => ({
    title: "Saiba mais",
    headerStyle: {
      backgroundColor: AppColors.ORANGE
    }
    /* header:navigationOptions.header,
    headerStyle: {
      backgroundColor: AppColors.ORANGE,
    },
    headerTintColor: navigationOptions.headerStyle.backgroundColor, */
  });

  constructor(props) {
    super(props);
    this.state = {
      index: 0
    };

    this._proximo = this._proximo.bind(this);
    this._anterior = this._anterior.bind(this);
    this._scrollToTop = this._scrollToTop.bind(this);
  }

  _proximo() {
    if (this.state.index + 1 == DORES.length) {
      this.setState({ index: 0 });
    } else {
      this.setState({ index: this.state.index + 1 });
    }
    setTimeout(this._scrollToTop, 300);
  }

  _anterior() {
    if (this.state.index == 0) {
      this.setState({ index: DORES.length - 1 });
    } else {
      this.setState({ index: this.state.index - 1 });
    }
    setTimeout(this._scrollToTop, 300);
  }

  _scrollToTop() {
    this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true });
  }

  render() {
    return (
      <Container style={styles.stage}>
        <StatusBar
          animated
          backgroundColor={AppColors.ORANGE_DARK}
          barStyle="light-content"
        />
        <ScrollView ref="_scrollView">
          <View style={styles.buttons}>
            <Button
              rounded
              light
              style={styles.button}
              onPress={this._anterior}
            >
              <Icon
                name="skip-backward"
                style={{ color: "#ffffff", fontSize: 30 }}
              />
            </Button>
            <Button rounded light style={styles.button} onPress={this._proximo}>
              <Icon
                name="skip-forward"
                style={{ color: "#ffffff", fontSize: 30 }}
              />
            </Button>
          </View>
          <Image
            source={DORES[this.state.index].imagem}
            style={styles.imageTop}
          />
          <View style={styles.body}>
            <Text style={styles.titulo}>{DORES[this.state.index].titulo}</Text>
            <Text style={styles.conteudo}>
              {DORES[this.state.index].descricao}
            </Text>
            <Text style={[styles.sobre, { fontFamily: "Helvetica-Neue" }]}>
              Referências
            </Text>
            <Text style={styles.sobre}>
              {DORES[this.state.index].referencia}
            </Text>
          </View>
         
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  stage: {
    backgroundColor: AppColors.ORANGE
  },
  body: {
    padding: AppMetrics.MD_SPACING
  },
  imageTop: {
    width: "100%",
    height: 200,
    resizeMode: "cover"
  },
  titulo: {
    color: AppColors.WHITE,
    textAlign: "center",
    fontFamily: "Helvetica-Neue",
    fontSize: 28,
    marginBottom: AppMetrics.MD_SPACING
  },
  conteudo: {
    color: AppColors.DARK,
    fontFamily: "Helvetica-Neue-LT-Std",
    textAlign: "justify",
    fontSize: AppMetrics.MD_TEXT,
    marginBottom: AppMetrics.MD_SPACING
  },
  sobre: {
    color: AppColors.WHITE,
    fontFamily: "Helvetica-Neue-LT-Std",
    fontSize: AppMetrics.SM_TEXT
  },
  buttons: {
    elevation: 6,
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: AppMetrics.MD_SPACING
  },
  button: {
    backgroundColor: AppColors.ORANGE_DARK,
    height: 60,
    width: 60,
    margin: AppMetrics.SM_SPACING
  }
});
