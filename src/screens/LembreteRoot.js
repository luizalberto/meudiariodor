import React, { Component } from "react";
import { Root } from "native-base";
import Lembrete from "./Lembrete";
import AppColors from "../styles/AppColors";

export default class LembreteRoot extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Lembrete",
    headerStyle: {
      backgroundColor: AppColors.RED
    }
  });

  render(){ return (<Root><Lembrete /></Root >) }
  
}

