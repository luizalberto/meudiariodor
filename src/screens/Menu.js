import {
  Body,
  Button,
  Container,
  Header,
  Icon,
  Right,
  Title
} from "native-base";
import React, { Component } from "react";
import {
  AsyncStorage,
  Image,
  ImageBackground,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View
} from "react-native";
import ButtonCustom from "../components/ButtonCustom";
import ButtonDial from "../components/ButtonDial";
import Dialog from "../components/Dialog";
import AppColors from "../styles/AppColors";
import AppMetrics from "../styles/AppMetrics";

export default class Menu extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Menus",
    header: null
  });

  constructor(props) {
    super(props);
    this.state = { editarNome: false, usuario: null };
    this.possuiNome = this.possuiNome.bind(this);
    this.salvarNome = this.salvarNome.bind(this);
    this.mostrarNome = this.mostrarNome.bind(this);
  }

  componentDidMount() {
    this.possuiNome();
  }

  async possuiNome() {
    let st = this.state;
    let usuario = await AsyncStorage.getItem("usuario");
    st.usuario = usuario;
    st.editarNome = usuario == null;
    this.setState(st);
  }

  async salvarNome() {
    let st = this.state;
    await AsyncStorage.setItem("usuario", this.state.nome);
    st.usuario = this.state.nome;
    st.editarNome = false;
    this.setState(st);
  }

  /*async imprimeNome() {
    let med = await AsyncStorage.getItem("MED1543821296102");
    let dor = await AsyncStorage.getItem("DOR1543818386127");

    await AsyncStorage.getAllKeys((err, keys) => {
      var rs = keys.filter(key => {
        return key.indexOf("DOR") != -1;
      });
      AsyncStorage.multiGet(rs, (err, itens) => {
        dores = itens.map(item => {
          let med = JSON.parse(item[1]);
          med.id = item[0];
          return med;
        });
        //s.listaMed = dores;
        console.log(dores);
      });
    });
  }*/

  mostrarNome() {
    //this.imprimeNome();
    let st = this.state;
    if (this.state.usuario) {
      return (
        <Header style={{ backgroundColor: AppColors.BLUE }}>
          <StatusBar
            backgroundColor={AppColors.DARK_BLUE}
            barStyle="light-content"
            translucent={false}
          />
          <Body>
            <Title style={{ color: AppColors.WHITE }}>
              {"Olá, " + this.state.usuario}
            </Title>
          </Body>
          <Right>
            <Button
              transparent
              onPress={() => {
                st.nome = st.usuario;
                st.editarNome = true;
                this.setState(st);
              }}
            >
              <Icon name="create" />
            </Button>
          </Right>
        </Header>
      );
    } else {
      return (
        <Header style={{ backgroundColor: AppColors.BLUE }}>
          <StatusBar
            backgroundColor={AppColors.DARK_BLUE}
            barStyle="light-content"
            translucent={false}
          />
        </Header>
      );
    }
  }

  render() {
    let st = this.state;
    return (
      <Container style={{ flex: 1 }}>
        {this.mostrarNome()}

        <ImageBackground
          style={styles.background}
          source={require("../assets/app_bg.png")}
        >
          <View style={styles.menus}>
            <ButtonCustom
              text="Diário"
              bgColor="blue"
              onPress={() => {
                this.props.navigation.navigate("Diario");
              }}
            />
            <ButtonCustom
              text="Saiba Mais"
              bgColor="orange"
              onPress={() => {
                this.props.navigation.navigate("SaibaMais");
              }}
            />
            <ButtonCustom
              text="Lembrete"
              bgColor="red"
              onPress={() => {
                this.props.navigation.navigate("LembreteRoot");
              }}
            />
            <ButtonCustom
              text="Relatório Médico"
              bgColor="dark-blue"
              onPress={() => {
                this.props.navigation.navigate("Relatorio");
              }}
            />
          </View>
          <Image
            style={styles.logoCristalia}
            source={require("../assets/logo_cristalia.png")}
          />

          {/* Editar Nome*/}
          <Dialog
            style={{ borderColor: AppColors.DARK_BLUE }}
            bgColor={AppColors.BLUE4}
            height={385}
            hide={!this.state.editarNome}
            BtnCloseHide={true}
            close={() => {
              this.props.navigation.navigate("Menu");
            }}
            bottons={
              <ButtonDial
                bgColor={AppColors.BLUE4}
                fontColor="#fff"
                text="Concluir"
                onPress={() => {
                  this.salvarNome();
                }}
              />
            }
          >
            <View
              style={{ flex: 1, flexDirection: "column", alignItems: "center" }}
            >
              <Text
                style={{
                  flex: 1,
                  textAlign: "center",
                  marginTop: 25,
                  color: "#fff",
                  fontSize: AppMetrics.XG_TEXT
                }}
              >
                SEJA BEM-VINDO AO
              </Text>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Image
                  style={{
                    flex: 1,
                    height: 120,
                    resizeMode: "contain",
                    marginTop: 40
                  }}
                  source={require("../assets/logo_white.png")}
                />
              </View>

              <Text
                style={{
                  flex: 1,
                  textAlign: "center",
                  marginTop: 20,
                  color: "#fff",
                  fontSize: AppMetrics.XG_TEXT
                }}
              >
                Informe seu nome:
              </Text>

              <TextInput
                underlineColorAndroid="transparent"
                value={this.nome}
                style={[
                  styles.input,
                  { width: "85%", marginTop: 15 }
                ]}
                onChangeText={value => {
                  st.nome = value;
                  this.setState(st);
                }}
              />
            </View>
          </Dialog>
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    paddingTop: 30
  },
  menus: {
    flex: 4,
    marginTop: 40,
    paddingBottom: 90
  },
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  logoCristalia: {
    flex: 1,
    paddingBottom: 50,
    width: 160,
    resizeMode: "contain"
  }
});
