import React, { Component } from "react";
import { View, ImageBackground, StatusBar } from "react-native";
import ButtonMenu from "../components/ButtonMenu";
import AppStyles from "../styles/AppStyles";
import AppColors from "../styles/AppColors";

export default class ScreenDiario extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Diário"
  });

  constructor(props) {
    super(props);
    this.state = {};
    StatusBar.setTranslucent(false);
  }

  callMainScreen() {
    const { navigate } = this.props.navigation;
    setTimeout(() => navigate("Menu"), 4000);
  }

  render() {
    return (
      <ImageBackground
        style={AppStyles.container}
        source={require("../assets/app_bg.png")}
      >
        <StatusBar
          backgroundColor={AppColors.DARK_BLUE}
          barStyle="light-content"
        />
        <View style={{ height: 220 }}>
          <ButtonMenu
            text="Registrar dor no corpo"
            onPress={() => {
              this.props.navigation.navigate("Corpo");
            }}
          />
          <ButtonMenu
            text="Registrar dor na cabeça"
            onPress={() => {
              this.props.navigation.navigate("Cabeca");
            }}
          />
          <ButtonMenu
            text="Histórico"
            onPress={() => {
              this.props.navigation.navigate("Historico");
            }}
          />
        </View>
      </ImageBackground>
    );
  }
}
