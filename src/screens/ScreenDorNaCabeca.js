import React, { Component } from "react";
import { Alert, AsyncStorage, ImageBackground } from "react-native";
import CabecaDor from "../components/CabecaDor";
import DataDor from "../components/DataDor";
import Medicamento from "../components/Medicamento";
import NivelDor from "../components/NivelDor";
import SintomasCabeca from "../components/SintomasCabeca";
import TabView, { ICON } from "../components/TabView";
import AppStyles from "../styles/AppStyles";
const KEY_DOR = "DOR";
export default class ScreenDorNaCabeca extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Registrar dor na cabeça"
  });

  constructor(props) {
    super(props);
    var data = new Date();
    var dia =
      data.getDate().toString().length == 1
        ? "0" + data.getDate()
        : data.getDate().toString();
    var mes =
      (data.getMonth() + 1).toString().length == 1
        ? "0" + (data.getMonth() + 1)
        : (data.getMonth() + 1).toString();
    var ano = data
      .getFullYear()
      .toString()
      .substring(2, 4);
    var hora =
      data.getHours().toString().length == 1
        ? "0" + data.getHours()
        : data.getHours().toString();
    var minuto =
      data.getMinutes().toString().length == 1
        ? "0" + data.getMinutes()
        : data.getMinutes().toString();
    this.state = {
      dor: {
        tipo: 2,
        diaInicio: dia,
        mesInicio: mes,
        anoInicio: ano,
        horaInicio: hora,
        minutoInicio: minuto
      }
    };
    this.updateState = this.updateState.bind(this);
  }

  componentDidMount() {
    var dor = this.props.navigation.getParam("dor");
    if (dor != undefined) {
      var st = this.state;
      st.dor = dor;
      this.setState(st);
    }
  }

  updateState(state) {
    this.setState(state);
  }

  async salvar() {
    var s = this.state;
    var id = new Date().getTime();
    try {
      var key =
        s.dor.id != null && s.dor.id != undefined ? s.dor.id : KEY_DOR + id;
      await AsyncStorage.setItem(key, JSON.stringify(s.dor));
      await AsyncStorage.setItem(
        "@medicamentos:" + id,
        JSON.stringify(s.dor.medicamentonome)
      );
    } catch (error) {}
    Alert.alert(
      "Salvo com sucesso",
      'A sua dor foi registrada e você poderá consultá-la através do menu "Diário > Histórico".'
    );

    s.dor = {};
    this.setState(s);
    this.props.navigation.navigate("Diario");
  }

  render() {
    return (
      <ImageBackground
        style={AppStyles.container}
        source={require("../assets/app_bg.png")}
      >
        <TabView
          concluir={() => {
            this.salvar();
          }}
          tabs={[
            { icon: ICON.data },
            { icon: ICON.cabeca },
            { icon: ICON.intensidade },
            { icon: ICON.caracteristicas },
            { icon: ICON.medicamento }
          ]}
          bodys={[
            <DataDor
              state={this.state}
              key={1}
              updateState={this.updateState}
            />,
            <CabecaDor
              state={this.state}
              key={2}
              updateState={this.updateState}
            />,
            <NivelDor
              state={this.state}
              key={3}
              updateState={this.updateState}
            />,
            <SintomasCabeca
              state={this.state}
              key={4}
              updateState={this.updateState}
            />,
            <Medicamento
              state={this.state}
              key={5}
              updateState={this.updateState}
            />
          ]}
        />
      </ImageBackground>
    );
  }
}
