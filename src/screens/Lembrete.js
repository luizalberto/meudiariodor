import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  ImageBackground,
  TextInput,
  FlatList,
  AsyncStorage,
  Alert,
  StatusBar
} from "react-native";
import ButtonMenu from "../components/ButtonMenu";
import Dialog from "../components/Dialog";
import ButtonDial from "../components/ButtonDial";
import AppMetrics from "../styles/AppMetrics";
import AppColors from "../styles/AppColors";
import PushNotification from "react-native-push-notification";
import { Picker, ActionSheet, Container } from "native-base";

PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function(token) {
    console.log("TOKEN:", token);
  },

  // (required) Called when a remote or local notification is opened or received
  onNotification: function(notification) {
    console.log("NOTIFICATION:", notification);

    // process the notification

    // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: true
});

const KEY_MED = "MED";

export default class Lembrete extends Component {

  constructor(props) {
    super(props);

    var data = new Date();
    var dia =
      data.getDate().toString().length == 1
        ? "0" + data.getDate()
        : data.getDate().toString();
    var mes =
      (data.getMonth() + 1).toString().length == 1
        ? "0" + (data.getMonth() + 1)
        : (data.getMonth() + 1).toString();
    var ano = data
      .getFullYear()
      .toString()
      .substring(2, 4);
    var hora =
      data.getHours().toString().length == 1
        ? "0" + data.getHours()
        : data.getHours().toString();
    var minuto =
      data.getMinutes().toString().length == 1
        ? "0" + data.getMinutes()
        : data.getMinutes().toString();
    this.state = {
      novoMed: false,
      listMed: false,
      medicamento: {
        diaInicio: dia,
        mesInicio: mes,
        anoInicio: ano,
        horaInicio: hora,
        minutoInicio: minuto
      },
      listaMed: [],
      remedios: []
    };
    this.openNovoMed = this.openNovoMed.bind(this);
    this.closeNovoMed = this.closeNovoMed.bind(this);
    this.openListMed = this.openListMed.bind(this);
    this.closeListMed = this.closeListMed.bind(this);
    this.salvar = this.salvar.bind(this);
    this.editar = this.editar.bind(this);
    this.excluir = this.excluir.bind(this);
    this.listar = this.listar.bind(this);
    this.getMedicamentos = this.getMedicamentos.bind(this);
  }

  openNovoMed() {
    var s = this.state;
    s.novoMed = true;
    this.setState(s);
    this.getMedicamentos().then(dados => console.log(dados));
  }

  async getMedicamentos() {
    await AsyncStorage.getAllKeys((err, keys) => {
      var rs = keys.filter(key => {
        return key.indexOf("@medicamentos:") != -1;
      });
      AsyncStorage.multiGet(rs, (err, itens) => {
        let medicamentos = itens.map(item => {
          let med = item[1].replace(/"/g, "");
          return med;
        });
        var s = this.state;
        s.remedios = medicamentos;
        this.setState(s);
      });
    });
  }

  closeNovoMed() {
    var s = this.state;
    s.novoMed = false;
    this.setState(s);
  }

  openListMed() {
    var s = this.state;
    s.listMed = true;
    this.listar();
    this.setState(s);
  }

  closeListMed() {
    var s = this.state;
    s.listMed = false;
    this.setState(s);
  }

  async salvar() {
    var s = this.state;
    var id = new Date().getTime();

    try {
      var key =
        s.medicamento.id != null && s.medicamento.id != undefined
          ? s.medicamento.id
          : KEY_MED + id;
      await AsyncStorage.setItem(key, JSON.stringify(s.medicamento));
    } catch (error) {
      //TODO
    }

    //Push notification
    var notificationTitle = "Hora da sua medicação";
    var dataInicio = new Date(
      parseInt("20" + s.medicamento.anoInicio),
      parseInt(s.medicamento.mesInicio, 10) - 1,
      parseInt(s.medicamento.diaInicio, 10),
      parseInt(s.medicamento.horaInicio, 10),
      parseInt(s.medicamento.minutoInicio, 10)
    );
    
    console.log(dataInicio)

    let notificationID = id.toString().substring(5);
    //dose, intervaloHora, intervaloDia
    var notificationMsg = {
      //... You can use all the options from localNotifications
      id: notificationID,
      number: 0,
      autoCancel: true,
      group: "diario_dor",
      title: notificationTitle,
      message:
        "Tomar " +
        s.medicamento.dose +
        " " +
        s.medicamento.medida +
        " do " +
        s.medicamento.nome, // (required)
      date: dataInicio,
      bigText:
        "Você precisa tomar " +
        s.medicamento.dose +
        " " +
        s.medicamento.medida +
        " do " +
        s.medicamento.nome +
        " agora mesmo!",
      repeatType: "time",
      repeatTime: parseInt(s.medicamento.intervaloHora * 60 * 60000)
    };

    PushNotification.localNotificationSchedule(notificationMsg);

    s.medicamento = {};
    this.setState(s);
    this.closeNovoMed();

    Alert.alert(
      "Salvo com sucesso",
      "Seu medicamento foi incluído com sucesso."
    );
  }

  editar(medic) {
    var s = this.state;
    s.medicamento = medic;
    this.setState(s);
    this.closeListMed();
    this.openNovoMed();
  }

  async excluir(id) {
    var notificationID = id.replace(KEY_MED, "").substring(5);

    await AsyncStorage.removeItem(id);
    PushNotification.cancelLocalNotifications({ id: notificationID });
    this.listar();
  }

  async listar() {
    var s = this.state;
    await AsyncStorage.getAllKeys((err, keys) => {
      var rs = keys.filter(key => {
        return key.indexOf(KEY_MED) != -1;
      });
      AsyncStorage.multiGet(rs, (err, itens) => {
        medicamentos = itens.map(item => {
          let med = JSON.parse(item[1]);
          med.id = item[0];
          return med;
        });
        s.listaMed = medicamentos;
        this.setState(s);
      });
    });
  }

  render() {
    var st = this.state;
    return (
      <Container style={styles.stage}>
      <StatusBar
          animated
          backgroundColor={AppColors.RED}
          barStyle="light-content"
        />
        <ImageBackground
          style={styles.background}
          source={require("../assets/app_bg_red.png")}
        >
          <View style={styles.menus}>
            <ButtonMenu
              showIcon={false}
              text="INCLUIR MEDICAMENTO"
              onPress={this.openNovoMed}
            />
            <ButtonMenu
              showIcon={false}
              text="LISTA DE MEDICAMENTOS"
              onPress={this.openListMed}
              style={{ marginBottom: 0 }}
            />
          </View>
          <Dialog
            bgColor="#f20404"
            style={{ borderColor: "#dd8888" }}
            hide={!this.state.novoMed}
            height={465}
            close={this.closeNovoMed}
            bottons={
              <ButtonDial
                bgColor="#f20404"
                fontColor="#fff"
                text="REGISTRAR"
                onPress={this.salvar}
              />
            }
          >
            <Text
              style={{
                textAlign: "center",
                marginTop: 25,
                color: "#fff",
                fontSize: AppMetrics.XG_TEXT
              }}
            >
              INCLUIR LEMBRETE DE MEDICAMENTO
            </Text>

            <View style={[styles.div, { height: 45, marginTop: 20 }]}>
              <Text style={[styles.text]}>Medicamento:</Text>
              <TextInput
                onSubmitEditing={() => {
                  this.doseTextInput.focus();
                }}
                onFocus={() => {
                  Alert.alert(
                    "Selecionar medicamento",
                    "Você deseja selecionar um medicamento já registrado no diário?",
                    [
                      { text: "Não" },
                      {
                        text: "Sim",
                        onPress: () => {
                          ActionSheet.show(
                            {
                              options: this.state.remedios,
                              cancelButtonIndex: 4,
                              destructiveButtonIndex: 3,
                              title: "Selecione um medicamento:"
                            },
                            buttonIndex => {
                              //this.setState({ clicked: BUTTONS[buttonIndex] });
                              st.medicamento.nome = this.state.remedios[
                                buttonIndex
                              ];
                              this.setState(st);
                            }
                          );
                        },
                        style: "cancel"
                      }
                    ],
                    { cancelable: false }
                  );
                }}
                returnKeyType="next"
                underlineColorAndroid="transparent"
                value={st.medicamento.nome}
                style={[
                  styles.input,
                  { width: 150, marginTop: -10, height: 42 }
                ]}
                onChangeText={value => {
                  st.medicamento.nome = value;
                  this.setState(st);
                }}
              />
            </View>
            <View style={[styles.div, { height: 45, marginTop: 10 }]}>
              <Text style={[styles.text, { marginLeft: 0 }]}>Dose:</Text>
              <TextInput
                ref={input => {
                  this.doseTextInput = input;
                }}
                returnKeyType="next"
                keyboardType="numeric"
                underlineColorAndroid="transparent"
                value={st.medicamento.dose}
                style={[
                  styles.input,
                  { width: 50, marginTop: -10, height: 42 }
                ]}
                onChangeText={value => {
                  st.medicamento.dose = value;
                  this.setState(st);
                }}
              />
              <Picker
                ref={input => {
                  this.dosagemPicker = input;
                }}
                value={st.medicamento.medida}
                selectedValue={st.medicamento.medida}
                onSubmitEditing={() => {
                  this.intervalohHoraTextInput.focus();
                }}
                returnKeyType="next"
                onValueChange={(itemValue, itemIndex) => {
                  st.medicamento.medida = itemValue;
                  this.setState(st);
                }}
                style={{
                  height: 42,
                  width: 80,
                  marginLeft: 10,
                  marginTop: -10,
                  backgroundColor: "#fff",
                  borderRadius: 5
                }}
              >
                <Picker.Item label="Selecione" value="" />
                <Picker.Item label="unidade(s)" value="unidade" />
                <Picker.Item label="ml" value="ml" />
                <Picker.Item label="gotas" value="gotas" />
              </Picker>
            </View>

            <View style={[styles.div, { height: 45, marginTop: 10 }]}>
              <Text style={[styles.text]}>A cada</Text>
              <TextInput
                ref={input => {
                  this.intervalohHoraTextInput = input;
                }}
                onSubmitEditing={() => {
                  this.intervalohDiaTextInput.focus();
                }}
                keyboardType="numeric"
                returnKeyType="next"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  { width: 45, height: 42, marginTop: -10, textAlign: "center" }
                ]}
                value={st.medicamento.intervaloHora}
                onChangeText={value => {
                  st.medicamento.intervaloHora = value;
                  this.setState(st);
                }}
              />
              <Text style={[styles.text]}> horas por</Text>
              <TextInput
                ref={input => {
                  this.intervalohDiaTextInput = input;
                }}
                onSubmitEditing={() => {
                  this.medicamentoDiaTextInput.focus();
                }}
                keyboardType="numeric"
                returnKeyType="next"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  { width: 45, marginTop: -10, height: 42, textAlign: "center" }
                ]}
                value={st.medicamento.intervaloDia}
                onChangeText={value => {
                  st.medicamento.intervaloDia = value;
                  this.setState(st);
                }}
              />
              <Text style={[styles.text]}> dias </Text>
            </View>

            <View style={[{ marginTop: 10, alignItems: "center" }]}>
              <Text style={[styles.text]}>Data de início:</Text>
            </View>

            <View
              style={[styles.div, { marginTop: 10, justifyContent: "center" }]}
            >
              <TextInput
                ref={input => {
                  this.medicamentoDiaTextInput = input;
                }}
                onSubmitEditing={() => {
                  this.medicamentoMesTextInput.focus();
                }}
                keyboardType="numeric"
                returnKeyType="next"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  { width: 45, marginTop: -10, height: 42, textAlign: "center" }
                ]}
                maxLength={2}
                value={st.medicamento.diaInicio}
                onChangeText={value => {
                  st.medicamento.diaInicio = value;
                  this.setState(st);
                }}
              />
              <Text style={[styles.text]}> /</Text>
              <TextInput
                ref={input => {
                  this.medicamentoMesTextInput = input;
                }}
                onSubmitEditing={() => {
                  this.medicamentoAnoTextInput.focus();
                }}
                keyboardType="numeric"
                returnKeyType="next"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  { width: 45, marginTop: -10, height: 42, textAlign: "center" }
                ]}
                keyboardType="decimal-pad"
                maxLength={2}
                value={st.medicamento.mesInicio}
                onChangeText={value => {
                  st.medicamento.mesInicio = value;
                  this.setState(st);
                }}
              />
              <Text style={[styles.text]}> /</Text>
              <TextInput
                ref={input => {
                  this.medicamentoAnoTextInput = input;
                }}
                onSubmitEditing={() => {
                  this.horaTextInput.focus();
                }}
                keyboardType="numeric"
                returnKeyType="next"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  { width: 45, marginTop: -10, height: 42, textAlign: "center" }
                ]}
                maxLength={2}
                value={st.medicamento.anoInicio}
                onChangeText={value => {
                  st.medicamento.anoInicio = value;
                  this.setState(st);
                }}
              />
            </View>
            <View style={[{ marginTop: 10, alignItems: "center" }]}>
              <Text style={[styles.text]}>às</Text>
            </View>
            <View
              style={[
                styles.div,
                { height: 45, marginTop: 10, justifyContent: "center" }
              ]}
            >
              <TextInput
                ref={input => {
                  this.horaTextInput = input;
                }}
                onSubmitEditing={() => {
                  this.minutoTextInput.focus();
                }}
                keyboardType="numeric"
                returnKeyType="next"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  { width: 45, marginTop: -10, height: 42, textAlign: "center" }
                ]}
                maxLength={2}
                value={st.medicamento.horaInicio}
                onChangeText={value => {
                  st.medicamento.horaInicio = value;
                  this.setState(st);
                }}
              />
              <Text style={[styles.text]}> : </Text>
              <TextInput
                ref={input => {
                  this.minutoTextInput = input;
                }}
                keyboardType="numeric"
                returnKeyType="done"
                underlineColorAndroid="transparent"
                style={[
                  styles.input,
                  {
                    width: 45,
                    marginTop: -10,
                    height: 42,
                    marginLeft: 0,
                    textAlign: "center"
                  }
                ]}
                maxLength={2}
                value={st.medicamento.minutoInicio}
                onChangeText={value => {
                  st.medicamento.minutoInicio = value;
                  this.setState(st);
                }}
              />
            </View>
            <View
              style={[
                styles.div,
                { marginTop: 0, marginBottom: 10, justifyContent: "center" }
              ]}
            >
              <Text style={{ color: "#fff", fontSize: AppMetrics.MD_TEXT }}>
                Lembre-se: só utilize medicamentos de acordo com a orientação do
                seu médico!
              </Text>
            </View>
          </Dialog>

          <Dialog
            bgColor="#f20404"
            borderColor="#FF6666"
            hide={!this.state.listMed}
            height={470}
            close={this.closeListMed}
          >
            <View>
              <View
                style={{
                  alignItems: "center",
                  marginTop: 10,
                  marginBottom: 25
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: 10,
                    color: "#fff",
                    fontSize: AppMetrics.XG_TEXT
                  }}
                >
                  LISTA DE MEDICAMENTOS
                </Text>
              </View>
              <FlatList
                data={this.state.listaMed}
                keyExtractor={item => item.id}
                renderItem={({ item }) => (
                  <View>
                    <View
                      style={[
                        styles.div,
                        { height: 48, marginTop: 10, alignItems: "flex-start" }
                      ]}
                    >
                      <Text
                        style={{
                          fontSize: AppMetrics.MD_TEXT,
                          color: "#fff",
                          width: 140
                        }}
                      >
                        {item.nome}
                      </Text>
                      <ButtonDial
                        text="Excluir"
                        bgColor="#f76767"
                        fontColor="#fff"
                        style={{ width: 65, alignSelf: "flex-end" }}
                        onPress={() => this.excluir(item.id)}
                      />
                      <ButtonDial
                        text="Editar"
                        bgColor="#f76767"
                        fontColor="#fff"
                        style={{ width: 60, alignSelf: "flex-end" }}
                        onPress={() => this.editar(item)}
                      />
                    </View>
                    <View
                      style={{
                        borderBottomColor: "#fff",
                        borderBottomWidth: 1,
                        borderStyle: "dashed"
                      }}
                    />
                  </View>
                )}
              />
            </View>
          </Dialog>
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    paddingTop: 30
  },
  menus: {
    padding: 0
  },
  div: {
    flexDirection: "row",
    paddingLeft: 15,
    paddingRight: 15
  },
  input: {
    color: "#000",
    fontSize: 18,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginLeft: 10,
    height: 25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 6,
    paddingRight: 6
  },
  text: {
    color: "#fff",
    fontSize: AppMetrics.MD_TEXT
  }
});
