import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  ImageBackground,
  StyleSheet,
  StatusBar,
  AsyncStorage
} from "react-native";
import AppColors from "../styles/AppColors";
import AppMetrics from "../styles/AppMetrics";
import AppValues from "../components/AppValues";

export default class Iniciativa extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "Iniciativa",
    header: null
  });

  constructor(props) {
    super(props);
    this.possuiNome = this.possuiNome.bind(this);
  }

  componentDidMount() {
    this.possuiNome().then(data => {
      if (data) {
        setTimeout(() => this.props.navigation.replace("Menu"), AppValues.FREEZE_TIME);
      } else {
        setTimeout(() => this.props.navigation.replace("Intro"), AppValues.FREEZE_TIME);
      }
    });
  }

  async possuiNome() {
    let usuario = await AsyncStorage.getItem("usuario");
    return usuario != null;
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden={true} />
        <ImageBackground
          style={styles.background}
          source={require("../assets/app_bg.png")}
        >
          <Text style={styles.texto}>
            Esta iniciativa educacional conta com apoio de:
          </Text>
          <Image
            tintColor={AppColors.DARK_BLUE}
            style={styles.logoCristalia}
            source={require("../assets/logo_cristalia.png")}
          />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    padding: AppMetrics.LG_SPACING,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  texto: {
    fontSize: AppMetrics.XG_TEXT,
    textAlign: "center"
  },
  logoCristalia: {
    width: 220,
    resizeMode: "contain"
  }
});
